// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// Internal Includes
// - none

// Library Includes
#include <range/v3/utility/concepts.hpp>

// Standard Includes
#include <type_traits>

namespace cxxgraph {

#if defined(_MSC_VER)
#define GRAPH_REQUIRES_(...) void* = nullptr
#define GRAPH_REQUIRES(...)
#else
#define GRAPH_REQUIRES_(...) CONCEPT_REQUIRES_(__VA_ARGS__)
#define GRAPH_REQUIRES(...) CONCEPT_REQUIRES(__VA_ARGS__)
#endif
//! For use as a function parameter
#define GRAPH_ENABLE_IF_(...) std::enable_if_t<(__VA_ARGS__), void*> = nullptr

//! For use as a trailing return type of a void function
#define GRAPH_ENABLE_IF(...) std::enable_if_t<(__VA_ARGS__)>

} // namespace cxxgraph
