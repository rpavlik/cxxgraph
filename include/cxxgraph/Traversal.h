// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// Internal Includes
#include "Base.h"

// Library Includes
// - none

// Standard Includes
#include <deque>
#include <set>
#include <vector>

namespace cxxgraph {

/*! Return value code from sub-operations of traversal.
 *
 * Indicates if the overall (common) traversal algorithm should continue, skip the "current" element, or terminate
 * operation completely.
 */
enum class ExecutionState
{
	Proceed, //!< Execution should proceed as usual
	Skip,    //!< Execution should skip processing this element
	Stop     //!< Execution should terminate processing all elements
};

//! Convert ExecutionState to a string, primarily for tracing.
constexpr const char* to_string(ExecutionState state)
{
	switch (state) {
	case ExecutionState::Proceed:
		return "Proceed";
	case ExecutionState::Skip:
		return "Skip";
	case ExecutionState::Stop:
		return "Stop";
	default:
		return "UNKNOWN";
	}
}

/*! CRTP base for all graph traversal strategies, based on the general graph scan
 * algorithm.
 *
 * See https://idea-instructions.com/graph-scan/ for a clever, nonverbal
 * description of the general process.
 *
 * Derived classes must provide:
 *
 * ```
 * template<typename F>
 * auto dequeueAndVisit(F&& earlyStoppingPredicate)
 *     -> std::tuple<vertex_const_descriptor_t, ExecutionState>;
 *
 * template<typename F>
 * auto enqueue(edge_const_descriptor_t e, vertex_const_descriptor_t prev,
 *     vertex_const_descriptor_t v, F&& earlyStoppingPredicate)
 *     -> ExecutionState;
 *
 * auto hasQueued() const -> bool;
 * ```
 *
 * Notes:
 *
 * - While `enqueue()` is given an edge descriptor, as well as the
 *   vertex to visit and its predecessor according to that edge,
 *   `dequeueAndVisit()` only needs to return the vertex to visit and execution
 *   state, so depending on the specific traversal desired, you do not
 *   necessarily need to store all three pieces of data.
 * - The first call to `enqueue()` will be trivial, with `e =
 *   edge_const_descriptor_t{}`, `predecessor == current`, and a dummy (always false)
 *   `earlyStoppingPredicate`.
 *
 */
template <typename Derived, typename Graph>
struct TraversalStrategyBase
{
	using graph_t = Graph;
	using vertex_const_descriptor_t = graph_vertex_const_descriptor_t<graph_t>;
	using edge_const_descriptor_t = graph_edge_const_descriptor_t<graph_t>;

	//! CRTP accessor for constant derived type.
	constexpr Derived const& derived() const noexcept { return static_cast<Derived const&>(*this); }

	//! CRTP accessor for derived type.
	constexpr Derived& derived() noexcept { return static_cast<Derived&>(*this); }

	/*! Called before enqueuing vertices connected to v.
	 *
	 * Effectively an event handler - can override in derived class.
	 */
	void beginVerticesConnectedTo(vertex_const_descriptor_t /*v*/) {}

	/*! Called after finished enqueuing vertices connected to v.
	 *
	 * Effectively an event handler - can override in derived class.
	 */
	void endVerticesConnectedTo(vertex_const_descriptor_t /*v*/) {}

private:
	// Avoids mistaken usage by derived classes:
	// see https://www.fluentcpp.com/2017/05/12/curiously-recurring-template-pattern/
	TraversalStrategyBase() = default;
	friend Derived;
};
namespace detail {
	struct AlwaysFalse
	{
		template <typename D>
		bool operator()(D const& /*d*/) const noexcept
		{
			return false;
		}
	};

	/*! Implementation function called by traverseImpl() to handle edge
	 * ranges associated with a currently-visited vertex.
	 */
	template <typename StrategyDerived, typename Graph, typename Rng, typename F>
	ExecutionState traverseEdgeRange(TraversalStrategyBase<StrategyDerived, Graph>& strategy,
	                                 graph_vertex_const_descriptor_t<Graph> v, Rng&& edgeRange,
	                                 F&& earlyStoppingPredicate)
	{
		auto& s = strategy.derived();
		for (auto p : edgeRange) {
			auto e = getEdgeDescriptor(p);
			auto otherEnd = getOtherEndOfEdge(p, v);
			auto ret = s.enqueue(e, v, otherEnd, std::forward<F>(earlyStoppingPredicate));
			if (ExecutionState::Stop == ret) {
				return ret;
			}
		}
		return ExecutionState::Proceed;
	}

	/*! Implementation function for all traversals.
	 */
	template <typename Graph, typename StrategyDerived, typename F>
	void traverseImpl(GraphBase<Graph> const& g, TraversalStrategyBase<StrategyDerived, Graph>& strategy,
	                  graph_vertex_const_descriptor_t<Graph> from, DirectedBehavior directedness,
	                  F&& earlyStoppingPredicate)
	{
		using vertex_const_descriptor_t = graph_vertex_const_descriptor_t<Graph>;
		using edge_const_descriptor_t = graph_edge_const_descriptor_t<Graph>;
		auto& s = strategy.derived();

		// Initialize first path: from vertex is its own predecessor.
		s.enqueue(edge_const_descriptor_t{}, from, from, AlwaysFalse{});

		while (s.hasQueued()) {
			vertex_const_descriptor_t v;
			ExecutionState state;
			std::tie(v, state) = s.dequeueAndVisit(std::forward<F>(earlyStoppingPredicate));
			if (ExecutionState::Skip == state) {
				// We already visited this vertex
				continue;
			}
			if (ExecutionState::Stop == state) {
				// We can exit now.
				return;
			}
			s.beginVerticesConnectedTo(v);
			// Follow all out-edges (or just edges if this is an
			// undirected graph)
			if ((directedness & DirectedBehavior::Directed) != DirectedBehavior(0)) {

				auto result = traverseEdgeRange(strategy, v, g.derived().vertexOutEdges(v),
				                                std::forward<F>(earlyStoppingPredicate));

				if (ExecutionState::Stop == result) {
					return;
				}
			}
			// Follow all in-edges (if requested)
			if constexpr (graph_is_bidirectional_directed_v<Graph>) {
				if ((directedness & DirectedBehavior::ReverseDirected) != DirectedBehavior(0)) {

					auto result = traverseEdgeRange(strategy, v, g.derived().vertexInEdges(v),
					                                std::forward<F>(earlyStoppingPredicate));

					if (ExecutionState::Stop == result) {
						return;
					}
				}
			}
			s.endVerticesConnectedTo(v);
		}
	}
} // namespace detail

template <typename Graph>
struct BreadthFirstTraversal : TraversalStrategyBase<BreadthFirstTraversal<Graph>, Graph>
{
	using base_t = TraversalStrategyBase<BreadthFirstTraversal<Graph>, Graph>;
	using graph_t = Graph;
	using vertex_const_descriptor_t = graph_vertex_const_descriptor_t<Graph>;
	using predecessor_map_t = VertexMap<Graph, vertex_const_descriptor_t>;
	using queued_map_t = VertexMap<Graph, bool>;
	using edge_const_descriptor_t = graph_edge_const_descriptor_t<Graph>;

	//! Constructor
	explicit BreadthFirstTraversal(Graph const& graph) : predecessors(graph), queued(graph) {}

	template <typename F>
	auto dequeueAndVisit(F && /*earlyStoppingPredicate*/) -> std::tuple<vertex_const_descriptor_t, ExecutionState>
	{
		auto front = queue.front();
		queue.pop_front();
		return std::make_tuple(front, ExecutionState::Proceed);
	}
	template <typename F>
	auto enqueue(edge_const_descriptor_t /*e*/, vertex_const_descriptor_t prev, vertex_const_descriptor_t v,
	             F&& earlyStoppingPredicate) -> ExecutionState
	{
		if (!isQueued(v)) {
			predecessors.set(v, prev);
			queued.set(v, true);
			queue.push_back(v);
			if (std::forward<F>(earlyStoppingPredicate)(v)) {
				return ExecutionState::Stop;
			}
		}
		return ExecutionState::Proceed;
	}
	auto hasQueued() const -> bool { return !queue.empty(); }
	/*! True if we have queued v.
	 *
	 * Note that all visited vertices have been queued.
	 */
	bool isQueued(vertex_const_descriptor_t v) const
	{
		return predecessors.get(v) != vertex_const_descriptor_t{};
		// return queued.get(v);
	}

	predecessor_map_t predecessors;
	queued_map_t queued;
	std::deque<vertex_const_descriptor_t> queue;
};

/*! Perform traversal of all vertices reachable from @pname from.
 *
 * This version of the traversal only works on graphs that are not bidirectional-directed.
 */
template <typename Graph, typename StrategyDerived>
inline auto traverse(GraphBase<Graph> const& g, TraversalStrategyBase<StrategyDerived, Graph>& strategy,
                     graph_vertex_const_descriptor_t<Graph> from)
    -> GRAPH_ENABLE_IF(!graph_is_bidirectional_directed_v<Graph>)
{
	// never stop early: always return false from predicate
	detail::traverseImpl(g, strategy, from, DirectedBehavior::Directed, detail::AlwaysFalse{});
}
/*! Perform traversal of the graph starting at @pname from with an
 * early-stopping predicate.
 *
 * This version of the traversal only works on graphs that are not bidirectional-directed.
 *
 * Traversal ends when the derived class's dequeueAndVisit() function
 * (which gets the earlyStoppingPredicate passed to it, which should
 * return true when given a vertex that should be the last one visited)
 * returns ExecutionState::Stop. Traversal also ends when all reachable
 * vertices are visited if ExecutionState::Stop is not returned before
 * then.
 */
template <typename Graph, typename StrategyDerived, typename F>
inline auto traverse(GraphBase<Graph> const& g, TraversalStrategyBase<StrategyDerived, Graph>& strategy,
                     graph_vertex_const_descriptor_t<Graph> from, F&& earlyStoppingPredicate)
    -> GRAPH_ENABLE_IF(!graph_is_bidirectional_directed_v<Graph> && !std::is_same<DirectedBehavior, F>::value)
{
	detail::traverseImpl(g, strategy, from, DirectedBehavior::Directed, std::forward<F>(earlyStoppingPredicate));
}

/*! Perform traversal of all vertices reachable from @pname from.
 *
 * This version of the traversal only works on graphs that are bidirectional-directed.
 */
template <typename Graph, typename StrategyDerived>
inline auto traverse(GraphBase<Graph> const& g, TraversalStrategyBase<StrategyDerived, Graph>& strategy,
                     graph_vertex_const_descriptor_t<Graph> from, DirectedBehavior directedness)
    -> GRAPH_ENABLE_IF(graph_is_bidirectional_directed_v<Graph>)
{
	// never stop early: always return false from predicate
	detail::traverseImpl(g, strategy, from, directedness, detail::AlwaysFalse{});
}
/*! Perform traversal of the graph starting at @pname from with an
 * early-stopping predicate and a directedness behavior.
 *
 * This version of the traversal only works on graphs that are bidirectional-directed.
 *
 * Traversal ends when the derived class's dequeueAndVisit() function
 * (which gets the earlyStoppingPredicate passed to it, which should
 * return true when given a vertex that should be the last one visited)
 * returns ExecutionState::Stop. Traversal also ends when all reachable
 * vertices are visited if ExecutionState::Stop is not returned before
 * then.
 */
template <typename Graph, typename StrategyDerived, typename F>
inline auto traverse(GraphBase<Graph> const& g, TraversalStrategyBase<StrategyDerived, Graph>& strategy,
                     graph_vertex_const_descriptor_t<Graph> from, DirectedBehavior directedness,
                     F&& earlyStoppingPredicate) -> GRAPH_ENABLE_IF(graph_is_bidirectional_directed_v<Graph>)
{
	detail::traverseImpl(g, strategy, from, directedness, std::forward<F>(earlyStoppingPredicate));
}
} // namespace cxxgraph
