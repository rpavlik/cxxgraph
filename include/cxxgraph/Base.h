// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// Internal Includes
#include "Fwd.h"
#include "Requires.h"

// Library Includes
#include <range/v3/core.hpp>
#include <range/v3/view/transform.hpp>

// Standard Includes
#include <tuple>
#include <type_traits>
#include <utility>

namespace cxxgraph {
//! @addtogroup entitytag
//! @{

//! True if T is vertex_tag.
template <typename T>
constexpr bool is_vertex_tag_v = std::is_same<T, vertex_tag>::value;

//! True if T is edge_tag.
template <typename T>
constexpr bool is_edge_tag_v = std::is_same<T, edge_tag>::value;

//! True if T is either edge_tag or vertex_tag.
template <typename T>
constexpr bool is_edge_or_vertex_tag_v = is_edge_tag_v<T> || is_vertex_tag_v<T>;

//! @}

namespace detail {
	//! @defgroup impl Implementation Details
	//! @{
	/*! Ultimate base of descriptor types.
	 *
	 * DescriptorTaggedBase<Tag> should be the only type directly inheriting from this.
	 */
	struct DescriptorUltimateBase
	{
	};

	/*! Ultimate tagged (edge or vertex specific) base of descriptor types.
	 *
	 * DescriptorBase<Derived, Tag> should be the only type directly inheriting from this.
	 */
	template <typename Tag>
	struct DescriptorTaggedBase : DescriptorUltimateBase
	{
		static_assert(is_edge_or_vertex_tag_v<Tag>);
	};

	template <typename Derived>
	struct VertexBase;

	template <typename Derived>
	struct EdgeBase;

	/*! Common CRTP base for EdgeBase<Derived> and VertexBase<Derived>
	 *
	 * Only EdgeBase<Derived> and VertexBase<Derived> should derive from this.
	 */
	template <typename Derived>
	struct EntityBase
	{
		using type = Derived;

		//! CRTP accessor for constant derived type.
		constexpr Derived const& derived() const noexcept { return static_cast<Derived const&>(*this); }

		//! CRTP accessor for derived type.
		constexpr Derived& derived() noexcept { return static_cast<Derived&>(*this); }

	private:
		// Avoids mistaken usage by derived classes:
		// see https://www.fluentcpp.com/2017/05/12/curiously-recurring-template-pattern/
		EntityBase() = default;
		friend Derived;
		friend VertexBase<Derived>;
		friend EdgeBase<Derived>;
	};

	//! @}

	/*! Descriptor constness.
	 *
	 * @ingroup storageimpl
	 */
	enum class Constness
	{
		Const,             //!< Descriptor is like a pointer to const
		NonConst,          //!< Descriptor is like a pointer to non-const
		NotConstIndicating //!< Descriptor does not differ between const and non-const
	};

	/*! CRTP base for descriptors.
	 *
	 * All descriptor types must derive from this type.
	 *
	 * @ingroup storageimpl
	 */
	template <typename Derived, typename Tag>
	class DescriptorBase : public DescriptorTaggedBase<Tag>
	{
	public:
		static_assert(is_edge_or_vertex_tag_v<Tag>, "Tag must be either edge_tag or vertex_tag.");
		using tag_t = Tag;
		using type = Derived;

		//! Default constness
		static constexpr Constness constness = Constness::NotConstIndicating;

		//! CRTP accessor for constant derived type.
		constexpr Derived const& derived() const noexcept { return static_cast<Derived const&>(*this); }
		//! CRTP accessor for derived type.
		constexpr Derived& derived() noexcept { return static_cast<Derived&>(*this); }

	private:
		// Avoids mistaken usage by derived classes:
		// see https://www.fluentcpp.com/2017/05/12/curiously-recurring-template-pattern/
		DescriptorBase() = default;
		friend Derived;
	};

	//! @addtogroup extendgraph
	//! @{
	/*! CRTP base for edge data types.
	 *
	 * All graph-specific edge structures must derive from this type.
	 */
	template <typename Derived>
	struct EdgeBase : EntityBase<Derived>
	{
		using type = Derived;

		//! CRTP accessor for constant derived type.
		constexpr Derived const& derived() const noexcept { return static_cast<Derived const&>(*this); }

		//! CRTP accessor for derived type.
		constexpr Derived& derived() noexcept { return static_cast<Derived&>(*this); }

	private:
		// Avoids mistaken usage by derived classes:
		// see https://www.fluentcpp.com/2017/05/12/curiously-recurring-template-pattern/
		EdgeBase() = default;
		friend Derived;
	};

	/*! CRTP base for vertex data types.
	 *
	 * All graph-specifc vertex structures must derive from this type.
	 */
	template <typename Derived>
	struct VertexBase : EntityBase<Derived>
	{
		using type = Derived;

		//! CRTP accessor for constant derived type.
		constexpr Derived const& derived() const noexcept { return static_cast<Derived const&>(*this); }

		//! CRTP accessor for derived type.
		constexpr Derived& derived() noexcept { return static_cast<Derived&>(*this); }

	private:
		// Avoids mistaken usage by derived classes:
		// see https://www.fluentcpp.com/2017/05/12/curiously-recurring-template-pattern/
		VertexBase() = default;
		friend Derived;
	};
	//! @}
} // namespace detail

namespace policy {

	/*! Fallback implementation of remove_constness.
	 *
	 * Used when the descriptor is already not const, or when it doesn't indicate constness at all.
	 *
	 * If your descriptor type distinguishes between const and non-const, you need to write your own overload of
	 * thus function.
	 *
	 * @ingroup storageimpl
	 * @relates detail::DescriptorBase
	 */
	template <typename Derived, typename Tag,
	          GRAPH_REQUIRES_(Derived::constness == detail::Constness::NotConstIndicating ||
	                          Derived::constness == detail::Constness::NonConst)>
	constexpr auto remove_constness(detail::DescriptorBase<Derived, Tag> const& d) -> Derived
	{
		return d.derived();
	}
} // namespace policy

/*! Type indicating source and target vertices for an edge.
 *
 * Every graph-specific edge type must include a member of this type named `connectivity`.
 *
 * @ingroup extendgraph
 */
template <typename VertexDescriptorType>
struct EdgeConnectivity
{
	//! Constructor
	constexpr EdgeConnectivity(VertexDescriptorType source_, VertexDescriptorType target_)
	    : source(source_), target(target_)
	{}

	/*! Source vertex.
	 *
	 * In undirected graphs, source <= target.
	 */
	VertexDescriptorType source;

	/*! Target vertex.
	 *
	 * In undirected graphs, source <= target.
	 */
	VertexDescriptorType target;
};

namespace policy {
	/*! Declaration of template that must be partially-specialized for
	 * each element storage policy.
	 *
	 * Should provide:
	 *
	 * ```c++
	 * static constexpr bool absolute_descriptors = ...; // if descriptors are pointers, etc.
	 * using container_t = ...;
	 * using const_descriptor_t = ...;
	 * using descriptor_t = ...; // same as const_descriptor_t if absolute_descriptors
	 * template <typename... Args>
	 * static descriptor_t emplace(container_t& elements, Args... a);
	 * static ValueType& at(container_t& elements, descriptor_t d)
	 * static ValueType const& at(container_t const& elements, const_descriptor_t d);
	 * static auto getRange(container_t const& elements);
	 * static auto getRange(container_t& elements);
	 * static auto getDescriptors(container_t const& elements);
	 *
	 * // Plus, the following if absolute_descriptors is true (and thus const_descriptor_t != descriptor_t)
	 * static ValueType const& at(const_descriptor_t d);
	 * static ValueType& at(descriptor_t d);
	 * ```
	 *
	 * Additionally, if descriptor_t and const_descriptor_t are not the same, you should ensure that descriptor_t
	 * can be implicitly converted to const_descriptor_t, and that you create an overload of remove_constness to
	 * turn const_descriptor_t into descriptor_t.
	 *
	 * @addtogroup storageimpl
	 */
	template <typename ElementStorage, typename Tag, typename ValueType>
	struct ElementStorageTraits;

	/*! Declaration of template that must be specialized for each
	 * element storage policy
	 */
	template <typename ElementStorage, typename Tag, typename ValueType, typename MapValueType>
	struct DescriptorMapTraits;

	/*! Compute the type of container that should be used for main
	 * element storage, given an element storage policy and the
	 * contained value type.
	 */
	template <typename ElementStorage, typename Tag, typename ValueType>
	using container_t = typename ElementStorageTraits<ElementStorage, Tag, ValueType>::container_t;

	/*! Compute a type for storing one MapValueType per element
	 * identifier, given an element storage policy and MapValueType.
	 */
	template <typename ElementStorage, typename Tag, typename ValueType, typename MapValueType>
	using descriptor_map_traits_t = DescriptorMapTraits<ElementStorage, Tag, ValueType, MapValueType>;

} // namespace policy

//! @addtogroup traits
//! @{
namespace traits {
	/*! Specializable template to obtain a graph config struct type from its
	 * type argument.
	 *
	 * In the base case, it just assumes that the argument is a graph
	 * config.
	 *
	 * If you create a new graph type, you should specialize this accordingly.
	 *
	 * If you want to **use** this, see graph_config_t for the sanitized alias version of this trait.
	 *
	 * @ingroup extend
	 */
	template <typename T>
	struct GetGraphConfigOracle
	{
		using type = T;
	};

	/*! Remove const, reference, and pointer from a type.
	 *
	 * Typically used within an alias declaration that looks up something in a trait.
	 *
	 * @ingroup traits
	 */
	template <typename T>
	using stripped_t = std::remove_cv_t<std::remove_pointer_t<std::remove_reference_t<std::remove_cv_t<T>>>>;

#ifndef GRAPH_DOCS

	namespace detail {

		template <typename T>
		struct IsEdgeConnectivityOracle : std::false_type
		{
		};

		template <typename VertexDescriptorType>
		struct IsEdgeConnectivityOracle<EdgeConnectivity<VertexDescriptorType>> : std::true_type
		{
		};

		template <typename T, typename = void>
		struct GetCTRPDerived
		{
			using type = T;
		};

		template <typename T>
		struct GetCTRPDerived<T, meta::void_<decltype(std::declval<T>().derived())>>
		{
			using type = stripped_t<decltype(std::declval<T>().derived())>;
		};
	} // namespace detail

#endif // !GRAPH_DOCS

	template <typename T>
	using crtp_derived_t = meta::_t<detail::GetCTRPDerived<stripped_t<T>>>;

	template <template <class> class CRTPBase, typename T>
	constexpr bool has_ctrp_base()
	{
		using Derived = crtp_derived_t<T>;
		return std::is_base_of<CRTPBase<Derived>, Derived>();
	}

	//! Is the type an EdgeConnectivity type?
	template <typename T>
	constexpr bool is_edge_connectivity_v = detail::IsEdgeConnectivityOracle<T>();

	//! Is the type a graph-specific entity (edge or vertex) type?
	template <typename T>
	constexpr bool is_entity_v = has_ctrp_base<::cxxgraph::detail::EntityBase, T>();

	//! Is the type a graph-specific edge type?
	template <typename T>
	constexpr bool is_edge_v = has_ctrp_base<::cxxgraph::detail::EdgeBase, T>();

	//! Is the type a graph-specific vertex type?
	template <typename T>
	constexpr bool is_vertex_v = has_ctrp_base<::cxxgraph::detail::VertexBase, T>();

	/*! Given a graph-related type, get the graph config struct type
	 * associated with it.
	 *
	 * T should either be a graph config struct itself, or some type
	 * parameterized on a graph config.
	 */
	template <typename T>
	using graph_config_t = typename GetGraphConfigOracle<stripped_t<T>>::type;

	template <typename T>
	using directedness_t = typename graph_config_t<T>::directedness_t;

	template <typename T, Directedness D>
	using is_directedness_equal_to_t = meta::equal_to<directedness_t<T>, directedness_constant_t<D>>;

	/*! Determines if the graph config struct supplied indicates an
	 * undirected graph.
	 *
	 * If using `config_maker_t`, pass `config::Undirected` as a type parameter to make an
	 * undirected graph,
	 *
	 * If manually making a graph config, make a type alias in your graph config for
	 * directedness_constant_t<Directedness::Undirected> named `directedness_t`, as follows:
	 *
	 * ```c++
	 * struct MyGraphConfig {
	 *     // ...
	 *     using directedness_t = directedness_constant_t<Directedness::Undirected>;
	 *     // ...
	 * };
	 * ```
	 *
	 * To make a (non-bidirectional) directed graph, see is_unidirectional_directed_t.
	 * To make a bidirectional-directed graph, see is_bidirectional_directed_t.
	 */
	template <typename T>
	using is_undirected_t = is_directedness_equal_to_t<T, Directedness::Undirected>;

	//! variable template alias for is_undirected_t
	template <typename T>
	constexpr bool is_undirected_v = is_undirected_t<T>::value;

	/*! Determines if the graph config struct supplied indicates a
	 * bidirectional-directed graph.
	 *
	 * A bidirectional-directed graph is a directed graph where in-edges are
	 * also tracked, to speed some traversals or to permit traversals that
	 * ignore edge direction.
	 *
	 * If using `config_maker_t`, pass `config::BidirectionalDirected` as a type parameter to make a
	 * bidirectional-directed graph,
	 *
	 * If manually making a graph config, make a type alias in your graph config for
	 * directedness_constant_t<Directedness::BidirectionalDirected> named `directedness_t`, as follows:
	 *
	 * ```c++
	 * struct MyGraphConfig {
	 *     // ...
	 *     using directedness_t = directedness_constant_t<Directedness::BidirectionalDirected>;
	 *     // ...
	 * };
	 * ```
	 *
	 * To make a (non-bidirectional) directed graph, see is_unidirectional_directed_t.
	 * To make an undirected graph, see is_undirected_t.
	 */
	template <typename T>
	using is_bidirectional_directed_t = is_directedness_equal_to_t<T, Directedness::BidirectionalDirected>;

	//! variable template alias for is_bidirectional_directed_t
	template <typename T>
	constexpr bool is_bidirectional_directed_v = is_bidirectional_directed_t<T>::value;

	/*! Determines if the graph config struct supplied indicates a
	 * unidirectional-directed graph.
	 *
	 * A unidirectional-directed graph is a directed graph where in-edges are
	 * not tracked, to reduce memory usage and speed insertion, at the cost of not being able to (easily) traverse
	 * edges in a reverse direction.
	 *
	 * If using `config_maker_t`, pass `config::UnidirectionalDirected` as a type parameter to make a
	 * unidirectional-directed graph,
	 *
	 * If manually making a graph config, make a type alias in your graph config for
	 * directedness_constant_t<Directedness::UnidirectionalDirected> named `directedness_t`, as follows:
	 *
	 * ```c++
	 * struct MyGraphConfig {
	 *     // ...
	 *     using directedness_t = directedness_constant_t<Directedness::UnidirectionalDirected>;
	 *     // ...
	 * };
	 * ```
	 *
	 * To make a bidirectional-directed graph, see is_bidirectional_directed_t.
	 * To make an undirected graph, see is_undirected_t.
	 */
	template <typename T>
	using is_unidirectional_directed_t = is_directedness_equal_to_t<T, Directedness::UnidirectionalDirected>;

	//! variable template alias for is_unidirectional_directed_t
	template <typename T>
	constexpr bool is_unidirectional_directed_v = is_unidirectional_directed_t<T>::value;

	/*! Element storage policy type for vertex data.
	 *
	 * This is a core type that must be chosen in a graph config.
	 *
	 * If using `config_maker_t`, you can specify this by passing
	 * `config::VertexStorage<desired_vertex_storage_policy>` as a type parameter.
	 *
	 * If manually making a graph config, do something like the following:
	 *
	 * ```c++
	 * struct MyGraphConfig {
	 *     // ...
	 *     using vertex_storage_policy = policy::VectorElementStoragePolicy;
	 *     // ...
	 * };
	 * ```
	 */
	template <typename T>
	using vertex_policy_t = typename graph_config_t<T>::vertex_storage_policy;

	/*! Element storage policy type for edge data.
	 *
	 * This is a core type that must be chosen in a graph config.
	 *
	 * If using `config_maker_t`, you can specify this by passing `config::EdgeStorage<desired_edge_storage_policy>`
	 * as a type parameter.
	 *
	 * If manually making a graph config, do something like the following:
	 *
	 * ```c++
	 * struct MyGraphConfig {
	 *     // ...
	 *     using edge_storage_policy = policy::VectorElementStoragePolicy;
	 *     // ...
	 * };
	 * ```
	 */
	template <typename T>
	using edge_policy_t = typename graph_config_t<T>::edge_storage_policy;

	/*! Type of properties stored internally per-vertex.
	 *
	 * This is a core type that must be chosen in your graph config.
	 *
	 * If using `config_maker_t`, you can specify this by passing `config::VertexProperties<your_type>` as a type
	 * parameter. Alternately, if you have no per-vertex data to store, pass `config::EmptyVertexProperties`.
	 *
	 * If manually making a graph config, do something like the following:
	 *
	 * ```c++
	 * struct MyGraphConfig {
	 *     // ...
	 *     using vertex_properties = MyPerVertexStruct;
	 *     // ...
	 * };
	 * ```
	 *
	 * If you have no per-vertex data to store, use cxxgraph::empty_properties
	 * (an alias of std::tuple<>)
	 */
	template <typename T>
	using vertex_properties_t = typename graph_config_t<T>::vertex_properties;

	/*! Type of properties stored internally per-edge.
	 *
	 * This is a core type that must be chosen in your graph config.
	 *
	 * If using `config_maker_t`, you can specify this by passing `config::EdgeProperties<your_type>` as a type
	 * parameter. Alternately, if you have no per-edge data to store, pass `config::EmptyEdgeProperties`.
	 *
	 * If manually making a graph config, do something like the following:
	 *
	 * ```c++
	 * struct MyGraphConfig {
	 *     // ...
	 *     using edge_properties = MyPerEdgeStruct;
	 *     // ...
	 * };
	 * ```
	 *
	 * If you have no per-edge data to store, use cxxgraph::empty_properties
	 * (an alias of std::tuple<>)
	 */
	template <typename T>
	using edge_properties_t = typename graph_config_t<T>::edge_properties;

#ifndef GRAPH_DOCS
	namespace detail {
		template <typename GraphConfig, typename Tag>
		struct StoragePolicyTrait;

		template <typename GraphConfig>
		struct StoragePolicyTrait<GraphConfig, edge_tag>
		{
			using type = edge_policy_t<GraphConfig>;
		};

		template <typename GraphConfig>
		struct StoragePolicyTrait<GraphConfig, vertex_tag>
		{
			using type = vertex_policy_t<GraphConfig>;
		};

		template <typename GraphConfig, typename Tag>
		struct PropertiesTrait;

		template <typename GraphConfig>
		struct PropertiesTrait<GraphConfig, edge_tag>
		{
			using type = edge_properties_t<GraphConfig>;
		};

		template <typename GraphConfig>
		struct PropertiesTrait<GraphConfig, vertex_tag>
		{
			using type = vertex_properties_t<GraphConfig>;
		};
	} // namespace detail

#endif // !GRAPH_DOCS

	/*! Gets the storage policy tag type for a given graph config and entity (vertex or edge).
	 *
	 * Note that this **doesn't** get the element storage trait associated with the policy; i.e. it gets
	 * `policy::VectorElementStoragePolicy`, not `policy::ElementStoragePolicy<policy::VectorElementStoragePolicy,
	 * Tag, ValueType>`. FOr that, use storage_traits_t
	 */
	template <typename GraphConfig, typename Tag>
	using storage_policy_t = meta::_t<detail::StoragePolicyTrait<graph_config_t<GraphConfig>, Tag>>;

	template <typename GraphConfig, typename Tag, typename ValueType>
	using storage_traits_t =
	    policy::ElementStorageTraits<storage_policy_t<graph_config_t<GraphConfig>, Tag>, Tag, ValueType>;

	/*! Gets the per-entity (vertex or edge) properties type for a given graph config.
	 */
	template <typename GraphConfig, typename Tag>
	using properties_t = meta::_t<detail::PropertiesTrait<graph_config_t<GraphConfig>, Tag>>;

	template <typename GraphConfig, typename Tag, typename ValueType>
	using const_descriptor_t = typename storage_traits_t<GraphConfig, Tag, ValueType>::const_descriptor_t;

	template <typename GraphConfig, typename Tag, typename ValueType>
	using descriptor_t = typename storage_traits_t<GraphConfig, Tag, ValueType>::descriptor_t;

	template <typename GraphConfig, typename Tag, typename ValueType, typename T>
	constexpr bool is_correct_descriptor_type_v =
	    std::is_same<stripped_t<T>, const_descriptor_t<GraphConfig, Tag, ValueType>>::value ||
	    std::is_same<stripped_t<T>, descriptor_t<GraphConfig, Tag, ValueType>>::value;

	template <typename GraphConfig, typename Tag, typename ValueType, typename MapValueType>
	using descriptor_map_traits_t =
	    policy::descriptor_map_traits_t<storage_policy_t<GraphConfig, Tag>, Tag, ValueType, MapValueType>;

	//! true if the provided type T is a descriptor for entities of tag Tag.
	template <typename Tag, typename T>
	constexpr bool
	    is_tagged_descriptor_type_v = std::is_base_of<cxxgraph::detail::DescriptorTaggedBase<Tag>, stripped_t<T>>();

	//! true if the provided type is any edge descriptor type
	template <typename T>
	constexpr bool is_edge_descriptor_v = is_tagged_descriptor_type_v<edge_tag, T>;

	//! true if the provided type is any vertex descriptor type
	template <typename T>
	constexpr bool is_vertex_descriptor_v = is_tagged_descriptor_type_v<vertex_tag, T>;
} // namespace traits
//! @}

/*! Gets an edge descriptor.
 *
 * Exists primarily for generic treatment of edge descriptors and (descriptor, edge) pairs.
 */
template <typename Derived>
constexpr auto&& getEdgeDescriptor(detail::DescriptorBase<Derived, edge_tag> const& d) noexcept
{
	return d.derived();
}

//! @overload
template <typename Derived>
constexpr auto&& getEdgeDescriptor(detail::DescriptorBase<Derived, edge_tag>&& d) noexcept
{
	return d.derived();
}

//! @overload
template <typename T1, typename T2>
constexpr auto&& getEdgeDescriptor(std::pair<T1, T2> const& descriptorEdgePair,
                                   GRAPH_ENABLE_IF_(traits::is_edge_descriptor_v<T1> &&
                                                    !traits::is_edge_descriptor_v<T2>)) noexcept
{
	return descriptorEdgePair.first;
}

//! @overload
template <typename T1, typename T2>
constexpr auto&& getEdgeDescriptor(std::pair<T1, T2> const& pair,
                                   GRAPH_ENABLE_IF_(traits::is_edge_descriptor_v<T2> &&
                                                    !traits::is_edge_descriptor_v<T1>)) noexcept
{
	return pair.second;
}

/*! Gets a reference to an edge type.
 *
 * Exists primarily for generic treatment of edges and (descriptor, edge) pairs.
 */
template <typename Derived>
constexpr auto&& getEdge(detail::EdgeBase<Derived> const& edge) noexcept
{
	return edge.derived();
}

//! @overload
template <typename Derived>
constexpr auto&& getEdge(detail::EdgeBase<Derived>& edge) noexcept
{
	return edge.derived();
}

//! @overload
template <typename T1, typename T2>
constexpr auto&& getEdge(std::pair<T1, T2> const& descriptorEdgePair, GRAPH_ENABLE_IF_(traits::is_edge_v<T2>)) noexcept
{
	return descriptorEdgePair.second;
}

//! @overload
template <typename T1, typename T2>
constexpr auto&& getEdge(std::pair<T1, std::reference_wrapper<T2>> const& descriptorEdgeRefPair,
                         GRAPH_ENABLE_IF_(traits::is_edge_v<T2>)) noexcept
{
	return descriptorEdgeRefPair.second.get();
}

/*! Gets a reference to edge connectivity.
 *
 * Exists primarily for generic treatment of edges and (descriptor, edge) pairs.
 */
template <typename VertexDescriptorType>
constexpr auto&& getConnectivity(EdgeConnectivity<VertexDescriptorType> const& connectivity) noexcept
{
	return connectivity;
}

//! @overload
template <typename T>
constexpr auto&& getConnectivity(T const& edgeOrDescriptorEdgePair,
                                 GRAPH_ENABLE_IF_(!traits::is_edge_connectivity_v<T>)) noexcept
{
	return getEdge(edgeOrDescriptorEdgePair).connectivity;
}

/*! Gets the source of an edge.
 *
 * In undirected graphs (where there is no distinction between source and target), the convention is that the
 * source is not greater than the target.
 */
template <typename VertexDescriptorType>
constexpr auto&& getSource(EdgeConnectivity<VertexDescriptorType> const& conn) noexcept
{
	return conn.source;
}

//! @overload
template <typename T>
constexpr auto&& getSource(T const& edgeOrDescriptorEdgePair,
                           GRAPH_ENABLE_IF_(!traits::is_edge_connectivity_v<T>)) noexcept
{
	return getSource(getConnectivity(edgeOrDescriptorEdgePair));
}

/*! Gets the target of an edge.
 *
 * In undirected graphs (where there is no distinction between source and target), the convention is that the
 * source is not greater than the target.
 */
template <typename VertexDescriptorType>
constexpr auto& getTarget(EdgeConnectivity<VertexDescriptorType> const& conn) noexcept
{
	return conn.target;
}

//! @overload
template <typename T>
constexpr auto& getTarget(T const& edgeOrDescriptorEdgePair,
                          GRAPH_ENABLE_IF_(!traits::is_edge_connectivity_v<T>)) noexcept
{
	return getTarget(getConnectivity(edgeOrDescriptorEdgePair));
}

/*! Gets a vertex descriptor.
 *
 * Exists primarily for generic treatment of vertex descriptors and (descriptor, vertex) pairs.
 */
template <typename Derived>
constexpr auto&& getVertexDescriptor(detail::DescriptorBase<Derived, vertex_tag> const& d) noexcept
{
	return d.derived();
}

//! @overload
template <typename Derived>
constexpr auto&& getVertexDescriptor(detail::DescriptorBase<Derived, vertex_tag>&& d) noexcept
{
	return d.derived();
}

//! @overload
template <typename T1, typename T2>
constexpr auto getVertexDescriptor(std::pair<T1, T2> const& descriptorVertexPair,
                                   GRAPH_ENABLE_IF_(traits::is_vertex_descriptor_v<T1> &&
                                                    !traits::is_vertex_descriptor_v<T2>)) noexcept
{
	return descriptorVertexPair.first;
}

//! @overload
template <typename T1, typename T2>
constexpr auto getVertexDescriptor(std::pair<T1, T2> const& pair,
                                   GRAPH_ENABLE_IF_(traits::is_vertex_descriptor_v<T2> &&
                                                    !traits::is_vertex_descriptor_v<T1>)) noexcept
{
	return pair.second;
}

/*! Gets a reference to a vertex type.
 *
 * Exists primarily for generic treatment of vertices and (descriptor, vertex) pairs.
 */
template <typename Derived>
constexpr auto&& getVertex(detail::VertexBase<Derived> const& v) noexcept
{
	return v.derived();
}

//! @overload
template <typename Derived>
constexpr auto&& getVertex(detail::VertexBase<Derived>& v) noexcept
{
	return v.derived();
}

//! @overload
template <typename T1, typename T2>
constexpr auto getVertex(std::pair<T1, T2> const& descriptorVertexPair,
                         GRAPH_ENABLE_IF_(traits::is_vertex_v<T2>)) noexcept
{
	return descriptorVertexPair.second;
}

//! @overload
template <typename T1, typename T2>
constexpr auto getVertex(std::pair<T1, std::reference_wrapper<T2>> const& descriptorVertexRefPair,
                         GRAPH_ENABLE_IF_(traits::is_vertex_v<T2>)) noexcept
{
	return descriptorVertexRefPair.second.get();
}

/*! Given an edge object and one of its two vertices, return the other one.
 *
 * First parameter may be anything you can call getSource() or getTarget() on, so:
 * - edge connectivity
 * - edge object
 * - pair of descriptor and edge object reference
 * - pair of descriptor and edge object reference_wrapper
 *
 * Throws if the vertex given isn't an endpoint of the edge.
 */
template <typename EdgeType, typename VertexDerived>
constexpr auto getOtherEndOfEdge(EdgeType&& e, detail::DescriptorBase<VertexDerived, vertex_tag> const& v)
{
	auto source = getSource(e);
	auto target = getTarget(e);
	if (v.derived() == source) {
		return target;
	} else if (v.derived() == target) {
		return source;
	} else {
		throw std::invalid_argument("The given vertex is not one end of the given "
		                            "edge.");
	}
}

/*! Gets a reference to an entity (that is, edge or vertex) type.
 *
 * Exists primarily for generic treatment of entities and (descriptor, entity) pairs.
 */
template <typename Derived>
constexpr auto&& getEntity(detail::EntityBase<Derived> const& entity) noexcept
{
	return entity.derived();
}

//! @overload
template <typename Derived>
constexpr auto&& getEntity(detail::EntityBase<Derived>& entity) noexcept
{
	return entity.derived();
}

//! @overload
template <typename T1, typename T2>
constexpr auto&& getEntity(std::pair<T1, T2> const& somethingEntityPair,
                           GRAPH_ENABLE_IF_(traits::is_entity_v<T2> && !traits::is_entity_v<T1>)) noexcept

{
	return somethingEntityPair.second;
}

//! @overload
template <typename T1, typename T2>
constexpr auto&& getEntity(std::pair<T1, T2> const& entitySomethingPair,
                           GRAPH_ENABLE_IF_(traits::is_entity_v<T1> && !traits::is_entity_v<T2>)) noexcept

{
	return entitySomethingPair.first;
}

//! Get the properties member of an entity (edge or vertex) structure.
template <typename Derived>
constexpr auto&& getProperties(detail::EntityBase<Derived>& entity) noexcept
{
	return entity.derived().properties;
}

//! @overload
template <typename Derived>
constexpr auto&& getProperties(detail::EntityBase<Derived> const& entity) noexcept
{
	return entity.derived().properties;
}

//! @overload
template <typename T>
constexpr auto&& getProperties(T&& entity, GRAPH_ENABLE_IF_(!traits::is_entity_v<T>)) noexcept
{
	return getProperties(getEntity(entity));
}

/*! Specializable struct with a collection of types and constants related to a graph.
 *
 * This should be specialized for each new graph type and provide:
 *
 * ```
 * using vertex_const_descriptor_t = ...;
 * using edge_const_descriptor_t = ...;
 * using edge_connectivity_t = ...;
 * static constexpr bool is_undirected_v = ...;
 * static constexpr bool is_bidirectional_directed_v = ...;
 * ```
 *
 * @ingroup extendgraph
 */
template <typename T>
struct GraphTraits;

/*! The type used to refer to a vertex in a graph.
 *
 * For example, this might be an index or an iterator type.
 */
template <typename GraphType>
using graph_vertex_const_descriptor_t = typename GraphTraits<GraphType>::vertex_const_descriptor_t;

/*! The type used to refer to an edge in a graph.
 *
 * For example, this might be an index or an iterator type.
 */
template <typename GraphType>
using graph_edge_const_descriptor_t = typename GraphTraits<GraphType>::edge_const_descriptor_t;

/*! The type used to store references (descriptors) of the endpoints of an edge in a graph.
 *
 * This is typically an EdgeConnectivity<> type.
 */
template <typename GraphType>
using graph_edge_connectivity_t = typename GraphTraits<GraphType>::edge_connectivity_t;

/*! True if a graph is undirected.
 */
template <typename GraphType>
constexpr bool graph_is_undirected_v = GraphTraits<GraphType>::is_undirected_v;

/*! True if a graph is bidirectional-directed.
 */
template <typename GraphType>
constexpr bool graph_is_bidirectional_directed_v = GraphTraits<GraphType>::is_bidirectional_directed_v;

/*! Returns a range of (descriptor, ref to element) pairs given the storage traits, a range of descriptors, and the
 * element container they correspond to.
 */
template <typename StorageTraits, typename ElementContainer, typename WrappedRange>
inline auto wrapDescriptorRange(WrappedRange&& range, ElementContainer& elements)
{
	using raw_elt_t = typename ElementContainer::value_type;
	using elt_t = std::add_lvalue_reference_t<std::conditional_t<std::is_const<ElementContainer>::value,
	                                                             // reference to const
	                                                             std::add_const_t<raw_elt_t>,
	                                                             // just regular reference
	                                                             raw_elt_t>>;
	return std::forward<WrappedRange>(range) | ranges::view::transform([&](auto desc) {
		       using pair_t = std::pair<decltype(desc), elt_t>;
		       return pair_t{desc, StorageTraits::at(elements, desc)};
	       });
}

//! @overload
template <typename StorageTraits, typename WrappedRange, GRAPH_REQUIRES_(StorageTraits::absolute_descriptors)>
inline auto wrapDescriptorRange(WrappedRange&& range)
{
	using elt_t = decltype(StorageTraits::at(range.front()));
	return std::forward<WrappedRange>(range) | ranges::view::transform([&](auto desc) {
		       using pair_t = std::pair<decltype(desc), elt_t>;
		       return pair_t{desc, StorageTraits::at(desc)};
	       });
}

template <typename Graph, typename ValueType>
class VertexMap
{

	using vertex_map_traits_t = typename Graph::template vertex_map_traits_t<ValueType>;
	using vertex_map_t = typename vertex_map_traits_t::descriptor_map_t;
	vertex_map_t map_;

public:
	using vertex_const_descriptor_t = graph_vertex_const_descriptor_t<Graph>;
	explicit VertexMap(Graph const& g) : map_(g.template createVertexMap<ValueType>()) {}
	void set(vertex_const_descriptor_t v, ValueType&& value)
	{
		vertex_map_traits_t::at(map_, v) = std::move(value);
	}

	void set(vertex_const_descriptor_t v, ValueType const& value) { vertex_map_traits_t::at(map_, v) = value; }

	auto&& get(vertex_const_descriptor_t v) const { return vertex_map_traits_t::at(map_, v); }

	auto getRange() -> decltype(vertex_map_traits_t::getRange(map_)) { return vertex_map_traits_t::getRange(map_); }

	auto getRange() const -> decltype(vertex_map_traits_t::getRange(map_))
	{
		return vertex_map_traits_t::getRange(map_);
	}
};

//! Flag for indicating how you follow edges, in the case of a bidirectional-directed graph.
enum class DirectedBehavior
{
	//! Only follow edges in the forward direction.
	Directed = (1 << 0),

	//! Only follow edges in the reverse direction.
	ReverseDirected = (1 << 1),

	//! Allow following both forward and backward edges.
	Bidirectional = (1 << 0) | (1 << 1)
};

/*! Logical "AND", for checking bitmasks.
 * @relates DirectedBehavior
 */
constexpr DirectedBehavior operator&(DirectedBehavior lhs, DirectedBehavior rhs)
{
	using underlying = std::underlying_type<DirectedBehavior>::type;
	return static_cast<DirectedBehavior>(static_cast<underlying>(lhs) & static_cast<underlying>(rhs));
}

/*! Logical "OR", for combining flags.
 * @relates DirectedBehavior
 */
constexpr DirectedBehavior operator|(DirectedBehavior lhs, DirectedBehavior rhs)
{
	using underlying = std::underlying_type<DirectedBehavior>::type;
	return static_cast<DirectedBehavior>(static_cast<underlying>(lhs) | static_cast<underlying>(rhs));
}

class GraphAccess
{
	template <typename Derived>
	friend class GraphBase;

	template <typename G, typename Descriptor>
	static auto&& getProperties(G&& g, Descriptor&& d)
	{
		return g.getMutable(d).properties;
	}
	template <typename G, typename Descriptor>
	static auto&& getConnectivity(G&& g, Descriptor&& d)
	{
		return g.get(d).connectivity;
	}
};
/*! CRTP base for graphs, providing common functionality and compile-time polymorphism.
 *
 * All graph types must derive from this.
 *
 * You can write a generic function acting on any graph type as follows (note the use of derived()):
 *
 * ```
 * template<typename Derived>
 * void myFunc(GraphBase<Derived> const& g) {
 *     doSomethingWith(g.derived());
 * }
 */
template <typename Derived>
class GraphBase
{
public:
	//! CRTP accessor for constant derived type.
	constexpr Derived const& derived() const noexcept { return static_cast<Derived const&>(*this); }
	//! CRTP accessor for derived type.
	constexpr Derived& derived() noexcept { return static_cast<Derived&>(*this); }

	using vertex_const_descriptor_t = graph_vertex_const_descriptor_t<Derived>;
	using edge_const_descriptor_t = graph_edge_const_descriptor_t<Derived>;

	/*! Given an edge and one of its two vertices, return the other one.
	 *
	 * Throws if the vertex given isn't an endpoint of the edge. May throw if the edge descriptor is invalid.
	 */
	auto getOtherEndOfEdge(edge_const_descriptor_t e, vertex_const_descriptor_t v) const
	{
		return cxxgraph::getOtherEndOfEdge(getConnectivity(e), v);
	}

	/*! Read-only access to the connectivity structure
	 * indicating the source and target vertices of an edge.
	 *
	 * May throw if e is not a valid edge descriptor.
	 */
	auto&& getConnectivity(edge_const_descriptor_t e) const { return GraphAccess::getConnectivity(derived(), e); }

	/*! Read-only access to the vertex descriptor for the
	 * "from"/source vertex of an edge.
	 *
	 * May throw if e is not a valid edge descriptor.
	 */
	auto getSource(edge_const_descriptor_t e) const { return getConnectivity(e).source; }

	/*! Read-only access to the vertex descriptor for the
	 * "to"/target vertex of an edge.
	 *
	 * May throw if e is not a valid edge descriptor.
	 */
	auto getTarget(edge_const_descriptor_t e) const { return getConnectivity(e).target; }

	//! Given an edge descriptor, get the properties.
	auto&& getProperties(edge_const_descriptor_t e) { return GraphAccess::getProperties(derived(), e); }

	//! @overload
	auto&& getProperties(edge_const_descriptor_t e) const { return GraphAccess::getProperties(derived(), e); }

	//! Given a vertex descriptor, get the properties.
	template <typename VertexDescriptor>
	auto&& getProperties(VertexDescriptor v)
	{
		return GraphAccess::getProperties(derived(), v);
	}

	//! @overload
	auto&& getProperties(vertex_const_descriptor_t v) const { return GraphAccess::getProperties(derived(), v); }

private:
	// Avoids mistaken usage by derived classes:
	// see https://www.fluentcpp.com/2017/05/12/curiously-recurring-template-pattern/
	GraphBase() = default;
	friend Derived;
};

} // namespace cxxgraph
