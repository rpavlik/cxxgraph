// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// Internal Includes
#include "Stream.h"
#include "Traversal.h"

// Library Includes
// - none

// Standard Includes
#include <sstream>

namespace cxxgraph {

/*! A "traversal strategy" that wraps a real traversal strategy, delegating operations but recording a string trace of
 * the procedure.
 *
 * Use by `auto myTraceStrategy = trace(realStrategy);` and then passing `myTraceStrategy` instead of `realStrategy`
 * to traverse()
 */
template <typename NestedStrategy, typename Graph>
struct TraversalTraceStrategy : TraversalStrategyBase<TraversalTraceStrategy<NestedStrategy, Graph>, Graph>
{
	using graph_t = Graph;
	using vertex_const_descriptor_t = cxxgraph::graph_vertex_const_descriptor_t<graph_t>;
	using edge_const_descriptor_t = cxxgraph::graph_edge_const_descriptor_t<graph_t>;
	std::reference_wrapper<NestedStrategy> nested;
	std::ostringstream os;

	constexpr explicit TraversalTraceStrategy(
	    cxxgraph::TraversalStrategyBase<NestedStrategy, Graph>& nested_) noexcept
	    : nested(std::ref(nested_.derived()))
	{}

	void beginVerticesConnectedTo(vertex_const_descriptor_t v)
	{
		os << v << " (\n";
		nested.get().beginVerticesConnectedTo(v);
	}

	void endVerticesConnectedTo(vertex_const_descriptor_t v)
	{
		os << ")\n";
		nested.get().endVerticesConnectedTo(v);
	}
	template <typename F>
	auto dequeueAndVisit(F&& earlyStoppingPredicate)
	    -> std::tuple<vertex_const_descriptor_t, cxxgraph::ExecutionState>
	{
		vertex_const_descriptor_t v;
		cxxgraph::ExecutionState state;
		std::tie(v, state) = nested.get().dequeueAndVisit(std::forward<F>(earlyStoppingPredicate));

		os << "visit " << v << " (result: " << to_string(state) << ")\n";
		return std::make_tuple(v, state);
	}
	template <typename F>
	auto enqueue(edge_const_descriptor_t e, vertex_const_descriptor_t prev, vertex_const_descriptor_t v,
	             F&& earlyStoppingPredicate) -> ExecutionState
	{
		auto state = nested.get().enqueue(e, prev, v, std::forward<F>(earlyStoppingPredicate));
		os << "  queued " << prev << " --" << e << "--> " << v;
		if (state != ExecutionState::Proceed) {
			os << " (result: " << to_string(state) << ")";
		}
		os << "\n";
		return state;
	}
	auto hasQueued() const -> bool { return nested.get().hasQueued(); }
};

//! Helper function to wrap a traversal strategy in a tracer, deducing all types.
template <typename NestedStrategy, typename Graph>
constexpr auto trace(TraversalStrategyBase<NestedStrategy, Graph>& strategy)
    -> TraversalTraceStrategy<NestedStrategy, Graph>
{
	return TraversalTraceStrategy<NestedStrategy, Graph>{strategy};
}
} // namespace cxxgraph
