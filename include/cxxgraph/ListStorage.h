// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// Internal Includes
#include "Base.h"

// Library Includes
#include <range/v3/core.hpp>
#include <range/v3/view/transform.hpp>

// Standard Includes
#include <list>
#include <map>

namespace cxxgraph::policy {
struct PointerDescriptorPrintable
{
	void const* ptr;
};

//! Type-safe wrapper indicating a pointer is an edge or vertex descriptor.
template <typename Tag, typename Pointer>
struct PointerDescriptor : cxxgraph::detail::DescriptorBase<PointerDescriptor<Tag, Pointer>, Tag>
{

	using tag_type = Tag;
	using pointer_t = Pointer;
	static_assert(is_edge_or_vertex_tag_v<Tag>, "Tag must be edge_tag or vertex_tag");

	static_assert(std::is_pointer<Pointer>());
	static constexpr bool is_const_v = std::is_const<std::remove_pointer_t<Pointer>>();
	static constexpr detail::Constness constness =
	    is_const_v ? detail::Constness::Const : detail::Constness::NonConst;

	//! Called by shared stream output functions.
	constexpr PointerDescriptorPrintable printable() const noexcept { return {ptr}; }

	using pointer_to_nonconst_t = std::add_pointer_t<std::remove_const_t<std::remove_pointer_t<pointer_t>>>;

	constexpr PointerDescriptor() noexcept = default;
	constexpr PointerDescriptor(Pointer p) noexcept : ptr(p) {}
	constexpr PointerDescriptor(PointerDescriptor<Tag, pointer_to_nonconst_t> const& p) noexcept : ptr(p.ptr) {}
	Pointer ptr = nullptr;

	template <typename Ptr, GRAPH_REQUIRES_(std::is_same<traits::stripped_t<Ptr>,
	                                                     traits::stripped_t<pointer_t>>::value&& is_const_v)>
	constexpr PointerDescriptor& operator=(PointerDescriptor<Tag, Ptr> const& p) noexcept
	{
		ptr = p.ptr;
		return *this;
	}

	/*! Equality comparison for PointerDescriptor<>.
	 *
	 * Delegates to underlying descriptor type's equality
	 * comparison.
	 */
	constexpr bool operator==(PointerDescriptor const& other) const noexcept { return ptr == other.ptr; }
	/*! Inequality comparison for PointerDescriptor<>.
	 *
	 * Delegates to underlying descriptor type's inequality
	 * comparison.
	 */
	constexpr bool operator!=(PointerDescriptor const& other) const noexcept { return ptr != other.ptr; }

	/*! Less-than comparison for PointerDescriptor<>.
	 *
	 * Delegates to underlying descriptor type's less-than
	 * comparison.
	 */
	constexpr bool operator<(PointerDescriptor const& other) const noexcept { return ptr < other.ptr; }
};

template <typename Tag, typename PointedToType>
inline PointerDescriptor<Tag, std::add_pointer_t<PointedToType>>
remove_constness(PointerDescriptor<Tag, PointedToType const*> d)
{
	return {const_cast<std::add_pointer_t<PointedToType>>(d.ptr)};
}

/*! Null pointer equality comparison function for PointerDescriptor<>.
 *
 * @relates PointerDescriptor
 */
template <typename Tag, typename Pointer>
constexpr bool operator==(std::nullptr_t, PointerDescriptor<Tag, Pointer> p)
{
	return p.ptr == nullptr;
}

//! @overload
template <typename Tag, typename Pointer>
constexpr bool operator==(PointerDescriptor<Tag, Pointer> p, std::nullptr_t)
{
	return p.ptr == nullptr;
}

/*! Null pointer inequality comparison function for PointerDescriptor<>.
 *
 * @relates PointerDescriptor
 */
template <typename Tag, typename Pointer>
constexpr bool operator!=(std::nullptr_t, PointerDescriptor<Tag, Pointer> p)
{
	return p.ptr != nullptr;
}

//! @overload
template <typename Tag, typename Pointer>
constexpr bool operator!=(PointerDescriptor<Tag, Pointer> p, std::nullptr_t)
{
	return p.ptr != nullptr;
}

/*! Swap function for PointerDescriptor<>, found via ADL.
 *
 * @relates PointerDescriptor
 */
template <typename Tag, typename Pointer>
inline void swap(PointerDescriptor<Tag, Pointer>& a, PointerDescriptor<Tag, Pointer>& b)
{
	using std::swap;
	swap(a.ptr, b.ptr);
}

/*! Element storage policy that stores elements in a std::list
 * and uses pointer-to-element as the descriptor.
 */
struct ListElementStoragePolicy;

// Traits associated with DequeElementStoragePolicy
template <typename Tag, typename ValueType>
struct ElementStorageTraits<ListElementStoragePolicy, Tag, ValueType>
{
	using container_t = std::list<ValueType>;
	using descriptor_t = PointerDescriptor<Tag, std::add_pointer_t<ValueType>>;
	using const_descriptor_t = PointerDescriptor<Tag, std::add_pointer_t<std::add_const_t<ValueType>>>;

	static constexpr bool absolute_descriptors = true;

	template <typename... Args>
	static descriptor_t emplace(container_t& elements, Args... a)
	{
		elements.emplace_back(std::forward<Args>(a)...);
		auto it = elements.end();
		--it;
		return {&(*it)};
	}

	static ValueType& at(descriptor_t d)
	{
		if (nullptr == d) {
			throw std::invalid_argument("Cannot access null descriptor.");
		}
		return *(d.ptr);
	}

	static ValueType& at(container_t& /*elements*/, descriptor_t d) { return at(d); }

	static ValueType const& at(const_descriptor_t d)
	{
		if (nullptr == d) {
			throw std::invalid_argument("Cannot access null descriptor.");
		}
		return *(d.ptr);
	}

	static ValueType const& at(container_t const& /*elements*/, const_descriptor_t d) { return at(d); }

	static auto getRange(container_t const& elements)
	{
		auto wrapPair = [](auto&& e) { return std::make_pair(const_descriptor_t{&e}, std::ref(e)); };
		return ranges::view::transform(elements, wrapPair);
	}

	static auto getRange(container_t& elements)
	{
		auto wrapPair = [](auto&& e) { return std::make_pair(descriptor_t{&e}, std::ref(e)); };
		return elements | ranges::view::transform(wrapPair);
	}

	static auto getDescriptors(container_t const& elements)
	{
		auto makeDescriptor = [](auto&& e) { return const_descriptor_t{&e}; };
		return elements | ranges::view::transform(makeDescriptor);
	}
};

// Descriptor map traits associated with
// ListElementStoragePolicy - descriptor maps are always
// maps in this case, at least for now.
template <typename Tag, typename ValueType, typename MapValueType>
struct DescriptorMapTraits<ListElementStoragePolicy, Tag, ValueType, MapValueType>
{
	using storage_traits_t = ElementStorageTraits<ListElementStoragePolicy, Tag, ValueType>;
	using const_descriptor_t = typename storage_traits_t::const_descriptor_t;
	using descriptor_map_t = std::map<const_descriptor_t, MapValueType>;
	using element_container_t = container_t<ListElementStoragePolicy, Tag, ValueType>;
	static descriptor_map_t create(element_container_t const& elements)
	{
		auto ret = descriptor_map_t{};
		for (auto d : storage_traits_t::getDescriptors(elements)) {
			ret[d];
		}
		return ret;
	}

	static MapValueType& at(descriptor_map_t& map, const_descriptor_t descriptor)
	{
		if (descriptor == nullptr) {
			throw std::invalid_argument("Cannot access descriptor map value at "
			                            "null descriptor.");
		}
		return map[descriptor];
	}

	static MapValueType const& at(descriptor_map_t const& map, const_descriptor_t descriptor)
	{
		if (descriptor == nullptr) {
			throw std::invalid_argument("Cannot access descriptor map value at "
			                            "null descriptor.");
		}
		auto it = map.find(descriptor);
		return it->second;
	}

	static auto&& getRange(descriptor_map_t& map) { return map; }

	static auto&& getRange(descriptor_map_t const& map) { return map; }
};

} // namespace cxxgraph::policy
