// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// Internal Includes
#include "Base.h"

// Library Includes
#include <range/v3/algorithm/find_if.hpp>
#include <range/v3/begin_end.hpp>
#include <range/v3/core.hpp>
#include <range/v3/size.hpp>
#include <range/v3/view/concat.hpp>
#include <range/v3/view/for_each.hpp>
#include <range/v3/view/remove_if.hpp>
#include <range/v3/view/transform.hpp>

// Standard Includes
#include <algorithm>
#include <iterator>
#include <vector>

namespace cxxgraph::adjacency_list {
template <typename GraphConfig>
class Graph;

template <typename GraphConfig>
struct Edge;

template <typename GraphConfig>
struct Vertex;

namespace detail {
	template <typename GraphConfig>
	struct Storage;
} // namespace detail

} // namespace cxxgraph::adjacency_list

#ifndef GRAPH_DOCS
namespace cxxgraph::traits {
// Specialization: Config for adjacency_list::Edge<GraphConfig> is
// GraphConfig.
template <typename GraphConfig>
struct GetGraphConfigOracle<adjacency_list::Edge<GraphConfig>> : GetGraphConfigOracle<GraphConfig>
{
};
// Specialization: Config for Vertex<GraphConfig> is
// GraphConfig.
template <typename GraphConfig>
struct GetGraphConfigOracle<adjacency_list::Vertex<GraphConfig>> : GetGraphConfigOracle<GraphConfig>
{
};
// Specialization: Config for adjacency_list::Graph<GraphConfig> is
// GraphConfig.
template <typename GraphConfig>
struct GetGraphConfigOracle<adjacency_list::Graph<GraphConfig>> : GetGraphConfigOracle<GraphConfig>
{
};
// Specialization: Config for adjacency_list::detail::Storage<GraphConfig> is
// GraphConfig.
template <typename GraphConfig>
struct GetGraphConfigOracle<adjacency_list::detail::Storage<GraphConfig>> : GetGraphConfigOracle<GraphConfig>
{
};

} // namespace cxxgraph::traits
#endif // !GRAPH_DOCS

namespace cxxgraph::adjacency_list::al_traits {
template <typename GraphConfig, typename Tag>
struct ElementTrait;

template <typename GraphConfig>
struct ElementTrait<GraphConfig, edge_tag>
{
	using type = Edge<GraphConfig>;
};

template <typename GraphConfig>
struct ElementTrait<GraphConfig, vertex_tag>
{
	using type = Vertex<GraphConfig>;
};

template <typename GraphConfig, typename Tag>
struct EntityTraits
{

	static_assert(is_edge_or_vertex_tag_v<Tag>, "Tag must be edge_tag or vertex_tag");
	using config_t = traits::graph_config_t<GraphConfig>;
	using policy_t = traits::storage_policy_t<config_t, Tag>;
	using element_t = meta::_t<ElementTrait<traits::graph_config_t<GraphConfig>, Tag>>;
	using storage_traits_t = traits::storage_traits_t<config_t, Tag, element_t>;

	using container_t = policy::container_t<policy_t, Tag, element_t>;

	/*! Type used to refer to an entity.
	 *
	 * May be invalidated on graph change, depending on your
	 * specified storage policy.
	 */
	using const_descriptor_t = typename storage_traits_t::const_descriptor_t;

	/*! Type used to refer to an entity in a non-const way.
	 *
	 * May be invalidated on graph change, depending on your
	 * specified storage policy.
	 */
	using descriptor_t = typename storage_traits_t::descriptor_t;
};

//! Structure stored for each entity.
template <typename GraphConfig, typename Tag>
using element_t = typename EntityTraits<GraphConfig, Tag>::element_t;

template <typename GraphConfig, typename Tag>
using storage_traits_t = typename EntityTraits<GraphConfig, Tag>::storage_traits_t;

template <typename GraphConfig>
using edge_traits_t = storage_traits_t<GraphConfig, edge_tag>;
template <typename GraphConfig>
using vertex_traits_t = storage_traits_t<GraphConfig, vertex_tag>;

template <typename GraphConfig, typename Tag>
using container_t = typename EntityTraits<GraphConfig, Tag>::container_t;

template <typename GraphConfig, typename Tag>
using const_descriptor_t = typename EntityTraits<GraphConfig, Tag>::const_descriptor_t;

template <typename GraphConfig, typename Tag>
using descriptor_t = typename EntityTraits<GraphConfig, Tag>::descriptor_t;

template <typename GraphConfig, typename Tag>
using descriptor_list_t = std::vector<const_descriptor_t<GraphConfig, Tag>>;

template <typename GraphConfig, typename Tag, typename MapValueType>
using descriptor_map_traits_t =
    traits::descriptor_map_traits_t<GraphConfig, Tag, element_t<GraphConfig, Tag>, MapValueType>;

template <typename GraphConfig, typename Tag>
struct absolute_descriptors
{
	using type = meta::bool_<storage_traits_t<GraphConfig, Tag>::absolute_descriptors>;
};

template <typename GraphConfig, typename Tag>
constexpr bool absolute_descriptors_v = storage_traits_t<GraphConfig, Tag>::absolute_descriptors;

template <typename GraphConfig, typename Tag, typename T>
constexpr bool is_correct_descriptor_type_v =
    traits::is_correct_descriptor_type_v<GraphConfig, Tag, element_t<GraphConfig, Tag>, T>;
/*! If the config is for a directed graph (possibly
 * bidirectional) with edge storage policy providing absolute
 * descriptors, we can store the out-edges directly in each
 * vertex data structure.
 */
template <typename GraphConfig>
constexpr bool
    has_internalized_edges_v = (!traits::is_undirected_v<GraphConfig>)&&(absolute_descriptors_v<GraphConfig, edge_tag>);

template <typename GraphConfig>
using adjacency_lists_traits_t =
    descriptor_map_traits_t<GraphConfig, vertex_tag, descriptor_list_t<GraphConfig, edge_tag>>;

template <typename GraphConfig>
using adjacency_lists_t = typename adjacency_lists_traits_t<GraphConfig>::descriptor_map_t;

} // namespace cxxgraph::adjacency_list::al_traits

#ifndef GRAPH_DOCS
namespace cxxgraph {

template <typename GraphConfig>
struct GraphTraits<adjacency_list::Graph<GraphConfig>>
{
	using graph_t = adjacency_list::Graph<GraphConfig>;
	using vertex_const_descriptor_t = adjacency_list::al_traits::const_descriptor_t<GraphConfig, vertex_tag>;
	using edge_const_descriptor_t = adjacency_list::al_traits::const_descriptor_t<GraphConfig, edge_tag>;
	using edge_connectivity_t = EdgeConnectivity<vertex_const_descriptor_t>;
	static constexpr bool is_undirected_v = traits::is_undirected_v<GraphConfig>;
	static constexpr bool is_bidirectional_directed_v = traits::is_bidirectional_directed_v<GraphConfig>;
};
} // namespace cxxgraph
#endif // !GRAPH_DOCS

namespace cxxgraph::adjacency_list {
//! Data stored internally for an edge.
template <typename GraphConfig>
struct Edge : cxxgraph::detail::EdgeBase<Edge<GraphConfig>>
{
	using vertex_const_descriptor_t = al_traits::const_descriptor_t<GraphConfig, vertex_tag>;

	using edge_properties_t = traits::properties_t<GraphConfig, edge_tag>;

	/*! Constructor with vertices, with additional args forwarded to
	 * properties constructor.
	 */
	template <typename... Args>
	Edge(vertex_const_descriptor_t source, vertex_const_descriptor_t target, Args... a)
	    : connectivity(source, target), properties(std::forward<Args>(a)...)
	{}

	// No copy-construction.
	Edge(Edge const&) = delete;

	// No copy-assignment.
	Edge& operator=(Edge const&) = delete;

	//! Move-construction OK.
	Edge(Edge&&) = default;

	//! Move-assignment OK.
	Edge& operator=(Edge&&) = default;

	//! Endpoints of this edge.
	EdgeConnectivity<vertex_const_descriptor_t> connectivity;

	//! Consumer-specified edge properties.
	edge_properties_t properties;

}; // namespace cxxgraph::adjacency_list

//! Data stored internally for a vertex.
template <typename GraphConfig>
struct Vertex : cxxgraph::detail::VertexBase<Vertex<GraphConfig>>
{
	using vertex_properties_t = traits::properties_t<GraphConfig, vertex_tag>;
	/*! Constructor, with all args forwarded to
	 * properties constructor.
	 */
	template <typename... Args>
	Vertex(Args... a) : properties(std::forward<Args>(a)...)
	{}

	// No copy-construction.
	Vertex(Vertex const&) = delete;

	// No copy-assignment.
	Vertex& operator=(Vertex const&) = delete;

	//! Move-construction OK.
	Vertex(Vertex&&) = default;

	//! Move-assignment OK.
	Vertex& operator=(Vertex&&) = default;

	using edge_data_t = std::conditional_t<al_traits::has_internalized_edges_v<GraphConfig>,
	                                       al_traits::container_t<GraphConfig, edge_tag>,
	                                       al_traits::descriptor_list_t<GraphConfig, edge_tag>>;

	vertex_properties_t properties;

	/*! A list of either edges or edge descriptors, depending on if we're
	 * internalizing edges. */
	edge_data_t outEdges;
};

#ifndef GRAPH_DOCS

namespace detail {
	using al_traits::const_descriptor_t;
	using al_traits::descriptor_t;
	using al_traits::edge_traits_t;
	using al_traits::has_internalized_edges_v;
	using al_traits::vertex_traits_t;
	using traits::is_undirected_v;

	/*! If a config is for a bidirectional-directed graph, or for an undirected
	 * graph where we are internalizing edges, we need to store a separate adjacency
	 * list for "in-edges".
	 */
	template <typename GC>
	constexpr bool needs_in_edges_v = (traits::is_bidirectional_directed_v<GC>) ||
	                                  (has_internalized_edges_v<GC> && is_undirected_v<GC>);

	template <typename... Ts>
	using list_t = meta::list<Ts...>;

	/*! Is a meta::list<T> if Condition is true, otherwise a meta::list<> */
	template <bool Condition, typename T>
	using conditional_list_t = std::conditional_t<Condition, list_t<T>, list_t<>>;

	template <typename GC>
	using external_edge_descriptor_list_t = std::vector<const_descriptor_t<GC, edge_tag>>;

	template <typename GC>
	struct Storage
	{
		// Always need a vertex container.
		using vertex_container_t = al_traits::container_t<GC, vertex_tag>;
		vertex_container_t vertices;
		using inner_storage_t =
		    meta::apply<meta::quote<std::tuple>,
		                meta::concat<
		                    // Will pick an element based on whether or not internalizing edges
		                    std::conditional_t<has_internalized_edges_v<GC>,

		                                       // Only need an external edge descriptor container if we are
		                                       // internalizing edges. This duplicate container works around a
		                                       // for_each range view not behaving the way we'd otherwise need.
		                                       list_t<external_edge_descriptor_list_t<GC>>,
		                                       // Only need an external edge container if we aren't
		                                       // internalizing edges.
		                                       list_t<al_traits::container_t<GC, edge_tag>>>,

		                    // Only need an in-edge container if we are a bidirectional or undirected graph.
		                    conditional_list_t<needs_in_edges_v<GC>, al_traits::adjacency_lists_t<GC>>>>;
		inner_storage_t inner;
	};

	template <typename GC>
	using storage_t = Storage<traits::graph_config_t<GC>>;

	template <typename T, typename GC>
	constexpr T& get(Storage<GC>& s) noexcept
	{
		return std::get<T>(s.inner);
	}

	template <typename T, typename GC>
	constexpr T const& get(Storage<GC> const& s) noexcept
	{
		return std::get<T>(s.inner);
	}

	template <typename GC, typename Storage>
	constexpr bool is_storage() noexcept
	{
		return std::is_same<storage_t<GC>, traits::stripped_t<Storage>>::value;
	}

	template <typename GC, typename Descriptor>
	constexpr bool is_correct_vertex_descriptor() noexcept
	{
		return al_traits::is_correct_descriptor_type_v<GC, vertex_tag, Descriptor>;
	}

	template <typename GC, typename Descriptor>
	constexpr bool is_correct_edge_descriptor() noexcept
	{
		return al_traits::is_correct_descriptor_type_v<GC, edge_tag, Descriptor>;
	}

	/*! Access edge storage related to a source vertex.
	 *
	 * Usable whether or not the graph has internalized edges.
	 * If we have internalized edges, it's OK to pass an invalid descriptor.
	 *
	 * Templated on storage to avoid duplication in providing const and
	 * non-const versions.
	 */
	template <typename GC, typename GraphStorage, typename VertexDescriptor>
	constexpr auto&&
	edgeStorage(GraphStorage&& g, VertexDescriptor const& source,
	            GRAPH_ENABLE_IF_(
	                is_storage<GC, GraphStorage>() &&
	                is_correct_vertex_descriptor<GC, VertexDescriptor>())) noexcept(!has_internalized_edges_v<GC>)
	{
		static_assert(is_storage<GC, GraphStorage>() && is_correct_vertex_descriptor<GC, VertexDescriptor>());
		if constexpr (has_internalized_edges_v<GC>) {
			return getVertex<GC>(g, source).outEdges;
		} else {
			// One overall storage for all edges.
			return get<al_traits::container_t<GC, edge_tag>>(g);
		}
	}

	//! @overload
	template <typename GC, typename GraphStorage>
	constexpr auto&&
	edgeStorage(GraphStorage&& g,
	            GRAPH_ENABLE_IF_(is_storage<GC, GraphStorage>())) noexcept(!has_internalized_edges_v<GC>)
	{
		return edgeStorage<GC>(g, const_descriptor_t<GC, vertex_tag>{});
	}

	/*! Access vertex storage
	 *
	 * Templated on storage to avoid duplication in providing const and
	 * non-const versions.
	 */
	template <typename GC, typename GraphStorage>
	constexpr auto&& vertexStorage(GraphStorage&& g, GRAPH_ENABLE_IF_(is_storage<GC, GraphStorage>())) noexcept
	{
		static_assert(is_storage<GC, GraphStorage>());
		return g.vertices;
	}

	template <typename GC, typename GraphStorage, typename R>
	inline auto wrapEdgeDescriptorRange(GraphStorage&& g, R&& r, GRAPH_ENABLE_IF_(is_storage<GC, GraphStorage>()))
	{
		if constexpr (has_internalized_edges_v<GC>) {
			return wrapDescriptorRange<edge_traits_t<GC>>(std::forward<R>(r));
		} else { // NOLINT
			return wrapDescriptorRange<edge_traits_t<GC>>(std::forward<R>(r), edgeStorage<GC>(g));
		}
	}

	template <typename GC, typename GraphStorage, typename VertexDescriptor, typename R>
	inline auto wrapEdgeDescriptorRange(GraphStorage&& g, VertexDescriptor&& v, R&& r,
	                                    GRAPH_ENABLE_IF_(is_storage<GC, GraphStorage>() &&
	                                                     is_correct_vertex_descriptor<GC, VertexDescriptor>()))
	{
		return wrapDescriptorRange<edge_traits_t<GC>>(std::forward<R>(r),
		                                              edgeStorage<GC>(g, std::forward<VertexDescriptor>(v)));
	}

	template <typename GC, typename GraphStorage, typename R>
	inline auto wrapVertexDescriptorRange(GraphStorage&& g, R&& r, GRAPH_ENABLE_IF_(is_storage<GC, GraphStorage>()))
	{
		return wrapDescriptorRange<al_traits::vertex_traits_t<GC>>(std::forward<R>(r), vertexStorage<GC>(g));
	}
	/*! If a graph is undirected, make sure that the source is not greater
	 * than the target.
	 */
	template <typename GC, typename VertexDescriptor>
	constexpr void
	normalizeEndpoints(VertexDescriptor& source, VertexDescriptor& target,
	                   GRAPH_ENABLE_IF_(is_correct_vertex_descriptor<GC, VertexDescriptor>())) noexcept
	{
		static_assert(is_correct_vertex_descriptor<GC, VertexDescriptor>());

		if constexpr (is_undirected_v<GC>) {
			if (target < source) {
				swap(source, target);
			}
		}
	}

	/*! Get a (possibly const) reference to edge_t from an edge
	 * descriptor.
	 *
	 * Templated on storage to avoid duplication in providing const and
	 * non-const versions.
	 */
	template <typename GC, typename GraphStorage, typename EdgeDescriptor>
	constexpr auto&& getEdge(GraphStorage&& g, EdgeDescriptor const& e,
	                         GRAPH_ENABLE_IF_(is_storage<GC, GraphStorage>() &&
	                                          is_correct_edge_descriptor<GC, EdgeDescriptor>()))
	{
		static_assert(is_storage<GC, GraphStorage>() && is_correct_edge_descriptor<GC, EdgeDescriptor>());
		if constexpr (has_internalized_edges_v<GC>) {
			// internalized edges implies absolute edge
			// descriptors
			return edge_traits_t<GC>::at(e);

		} else { // NOLINT
			return edge_traits_t<GC>::at(edgeStorage<GC>(g), e);
		}
	}

	/*! Get a (possibly const) reference to vertex_t from a vertex
	 * descriptor.
	 *
	 * Templated on storage to avoid duplication in providing const and
	 * non-const versions.
	 */
	template <typename GC, typename GraphStorage, typename VertexDescriptor>
	inline auto&& getVertex(GraphStorage&& g, VertexDescriptor const& v,
	                        GRAPH_ENABLE_IF_(is_storage<GC, GraphStorage>() &&
	                                         is_correct_vertex_descriptor<GC, VertexDescriptor>()))
	{
		static_assert(is_storage<GC, GraphStorage>() && is_correct_vertex_descriptor<GC, VertexDescriptor>());
		return al_traits::vertex_traits_t<GC>::at(vertexStorage<GC>(g), v);
	}

	/*! Access outgoing edges from a vertex: either an adjacency list (if we
	 * aren't internalizing edges), or the edge container directly.
	 *
	 * Templated on storage to avoid duplication in providing const and
	 * non-const versions.
	 */
	template <typename GC, typename GraphStorage, typename VertexDescriptor>
	constexpr auto&& vertexOutgoingEdges(GraphStorage&& g, VertexDescriptor&& v,
	                                     GRAPH_ENABLE_IF_(is_storage<GC, GraphStorage>() &&
	                                                      is_correct_vertex_descriptor<GC, VertexDescriptor>()))
	{
		static_assert(is_storage<GC, GraphStorage>() && is_correct_vertex_descriptor<GC, VertexDescriptor>());
		return getVertex<GC>(g, v).outEdges;
	}

	/*! Access adjacency list of incoming edges to a vertex.
	 *
	 * Templated on storage to avoid duplication in providing const and
	 * non-const versions.
	 */
	template <typename GC, typename GraphStorage, typename VertexDescriptor>
	constexpr auto&& vertexIncomingEdges(GraphStorage&& g, VertexDescriptor const& v,
	                                     GRAPH_ENABLE_IF_(is_storage<GC, GraphStorage>() &&
	                                                      is_correct_vertex_descriptor<GC, VertexDescriptor>() &&
	                                                      needs_in_edges_v<GC>))
	{
		static_assert(is_storage<GC, GraphStorage>() && is_correct_vertex_descriptor<GC, VertexDescriptor>() &&
		              needs_in_edges_v<GC>);
		using adj_lists_traits_t = al_traits::adjacency_lists_traits_t<GC>;

		return adj_lists_traits_t::at(get<typename adj_lists_traits_t::descriptor_map_t>(g), v);
	}

	/*! Gets a range where each element is a (edge_descriptor, edge_t
	 * (const) &) pair for each outgoing edge from the vertex.
	 *
	 * Templated on storage to avoid duplication in providing const and
	 * non-const versions.
	 */
	template <typename GC, typename GraphStorage, typename VertexDescriptor>
	inline auto vertexOutgoingEdgeRange(GraphStorage&& g, VertexDescriptor const& v,
	                                    GRAPH_ENABLE_IF_(is_storage<GC, GraphStorage>() &&
	                                                     is_correct_vertex_descriptor<GC, VertexDescriptor>()))
	{
		static_assert(is_storage<GC, GraphStorage>() && is_correct_vertex_descriptor<GC, VertexDescriptor>());
		if constexpr (is_undirected_v<GC> && has_internalized_edges_v<GC>) {

			// Undirected and internalized - must combine ranges
			auto outEdges = edge_traits_t<GC>::getRange(vertexOutgoingEdges<GC>(g, v));
			auto inEdges = wrapVertexDescriptorRange<GC>(g, v, vertexIncomingEdges<GC>(g, v));
			auto isSelfEdge = [&](auto const& p) { return getSource(p) == v; };
			return ranges::view::concat(outEdges, inEdges | ranges::view::remove_if(isSelfEdge));

		} else if constexpr (has_internalized_edges_v<GC>) {

			// Directed and internalized
			return edge_traits_t<GC>::getRange(vertexOutgoingEdges<GC>(g, v));

		} else { // NOLINT

			// Not internalized
			return wrapEdgeDescriptorRange<GC>(g, vertexOutgoingEdges<GC>(g, v));
		}
	}

	/*! Gets a range of the edge_descriptor for each outgoing edge from the
	 * vertex.
	 *
	 * Templated on storage to avoid duplication in providing const and
	 * non-const versions.
	 */
	template <typename GC, typename GraphStorage, typename VertexDescriptor>
	inline auto vertexOutgoingEdgeDescriptors(
	    GraphStorage&& g, VertexDescriptor const& v,
	    GRAPH_ENABLE_IF_(is_storage<GC, GraphStorage>() && is_correct_vertex_descriptor<GC, VertexDescriptor>()))
	{
		static_assert(is_storage<GC, GraphStorage>() && is_correct_vertex_descriptor<GC, VertexDescriptor>());
		if constexpr (is_undirected_v<GC> && has_internalized_edges_v<GC>) {

			// Undirected and internalized - must combine ranges
			auto outEdges = edge_traits_t<GC>::getDescriptors(vertexOutgoingEdges<GC>(g, v));
			auto inEdges = vertexIncomingEdges<GC>(g, v);
			auto isSelfEdge = [&](auto const& e) { return getSource(getEdge<GC>(g, e)) == v; };
			return ranges::view::concat(outEdges, inEdges | ranges::view::remove_if(isSelfEdge));

		} else if constexpr (has_internalized_edges_v<GC>) { // NOLINT

			// Directed and internalized
			return edge_traits_t<GC>::getDescriptors(vertexOutgoingEdges<GC>(g, v));

		} else { // NOLINT

			// Not internalized
			// so the outgoing edges is already an adjacency list
			return vertexOutgoingEdges<GC>(g, v);
		}
	}

	/*! Gets the external edge_descriptor storage for the graph.
	 *
	 * Only used when we internalize edges.
	 *
	 * Templated on storage to avoid duplication in providing const and
	 * non-const versions.
	 */
	template <typename GC, typename GraphStorage>
	constexpr auto&& edgeDescriptorStorage(GraphStorage&& g,
	                                       GRAPH_ENABLE_IF_(is_storage<GC, GraphStorage>() &&
	                                                        has_internalized_edges_v<GC>)) noexcept
	{
		static_assert(is_storage<GC, GraphStorage>() && has_internalized_edges_v<GC>);
		return get<external_edge_descriptor_list_t<GC>>(g);
	}

	/*! Gets a range of all (edge_descriptor, edge (const) &) pairs in the
	 * graph.
	 *
	 * Templated on storage to avoid duplication in providing const and
	 * non-const versions.
	 */
	template <typename GC, typename GraphStorage>
	constexpr auto edges(GraphStorage&& g,
	                     GRAPH_ENABLE_IF_(is_storage<GC, GraphStorage>())) noexcept(!has_internalized_edges_v<GC>)
	{
		static_assert(is_storage<GC, GraphStorage>());
		if constexpr (has_internalized_edges_v<GC>) {
			return wrapEdgeDescriptorRange<GC>(g, edgeDescriptorStorage<GC>(g));
		} else { // NOLINT

			// Not internalized
			return edge_traits_t<GC>::getRange(edgeStorage<GC>(g));
		}
	}

	/*! Gets a range of all edge_descriptors in the graph.
	 *
	 * Templated on storage to avoid duplication in providing const and
	 * non-const versions.
	 */
	template <typename GC, typename GraphStorage>
	inline auto edgeDescriptors(GraphStorage&& g, GRAPH_ENABLE_IF_(is_storage<GC, GraphStorage>()))
	{
		static_assert(is_storage<GC, GraphStorage>());
		if constexpr (has_internalized_edges_v<GC>) {
			return ranges::view::all(edgeDescriptorStorage<GC>(g));
		} else { // NOLINT

			// Not internalized
			return edge_traits_t<GC>::getDescriptors(edgeStorage<GC>(g));
		}
	}

	/*! Gets the total number of edges in a graph.
	 */
	template <typename GC>
	constexpr auto numEdges(storage_t<GC> const& g) -> std::size_t
	{
		if constexpr (has_internalized_edges_v<GC>) {
			// Internalized edges
			auto numOutEdges = [](auto& v) { return ranges::size(v.outEdges); };
			return ranges::accumulate(vertexStorage<GC>(g) | ranges::view::transform(numOutEdges), 0);
		} else { // NOLINT

			// Not internalized
			return edgeStorage<GC>(g).size();
		}
	}

	/*! Gets the total number of vertices in a graph.
	 */
	template <typename GC>
	constexpr auto numVertices(storage_t<GC> const& g) noexcept -> std::size_t
	{
		return vertexStorage<GC>(g).size();
	}

	/*! True if an edge from source to target already exists.
	 */
	template <typename GC>
	constexpr auto hasEdge(storage_t<GC> const& g, const_descriptor_t<GC, vertex_tag> source,
	                       const_descriptor_t<GC, vertex_tag> target) -> bool
	{
		normalizeEndpoints<GC>(source, target);
		auto r = vertexOutgoingEdgeRange<GC>(g, source);
		auto it = ranges::find_if(r, [&](auto p) { return getTarget(p) == target; });
		return it != ranges::end(r);
	}

	//! Add a vertex, returning the descriptor.
	template <typename GC, typename... Args>
	inline auto addVertex(storage_t<GC>& g, Args&&... a) -> descriptor_t<GC, vertex_tag>
	{
		auto ret = al_traits::vertex_traits_t<GC>::emplace(vertexStorage<GC>(g), std::forward<Args>(a)...);
		if constexpr (needs_in_edges_v<GC>) {
			// Need this call to extend the list of
			// incoming-edge-lists.
			vertexIncomingEdges<GC>(g, ret);
		}
		return ret;
	}

	/*! Add an edge, returning the descriptor.
	 *
	 * @throws std::logic_error if an edge from source to target already exists in this graph.
	 */
	template <typename GC, typename... Args>
	inline auto addEdge(storage_t<GC>& g, const_descriptor_t<GC, vertex_tag> source,
	                    const_descriptor_t<GC, vertex_tag> target, Args&&... a) -> descriptor_t<GC, edge_tag>
	{
		if (hasEdge<GC>(g, source, target)) {
			throw std::logic_error("Already have such an edge!");
		}
		normalizeEndpoints<GC>(source, target);

		using cxxgraph::policy::remove_constness;

		auto ret = edge_traits_t<GC>::emplace(edgeStorage<GC>(g, remove_constness(source)), source, target,
		                                      std::forward<Args>(a)...);
		if constexpr (has_internalized_edges_v<GC>) {
			// Need to stash this descriptor in our external descriptor storage.
			edgeDescriptorStorage<GC>(g).push_back(ret);
		} else {
			// Edges aren't internalized, so we need to manually add
			// to out edges list.
			vertexOutgoingEdges<GC>(g, remove_constness(source)).push_back(ret);
			if (is_undirected_v<GC>) {
				// Undirected: add to out-edges of target as
				// well.
				vertexOutgoingEdges<GC>(g, remove_constness(target)).push_back(ret);
			}
		}
		if constexpr (needs_in_edges_v<GC>) {
			// Bi-directed, or internalized undirected: add to in
			// edges list.
			vertexIncomingEdges<GC>(g, remove_constness(target)).push_back(ret);
		}
		return ret;
	}

} // namespace detail

#endif // !GRAPH_DOCS

/*! A graph that store adjacency data in a list per-vertex.
 *
 * @tparam GraphConfig a struct containing nested types
 * vertex_storage_policy, vertex_properties,
 * edge_storage_policy, and edge_properties.
 */
template <typename GraphConfig>
class Graph : public cxxgraph::GraphBase<Graph<GraphConfig>>
{
	using edge_traits_t = al_traits::edge_traits_t<GraphConfig>;
	using vertex_traits_t = al_traits::vertex_traits_t<GraphConfig>;

	using storage_t = detail::storage_t<GraphConfig>;

	friend class cxxgraph::GraphAccess;

public:
	//! Overall storage policy struct.
	using graph_config_t = GraphConfig;

	/*! Type used to refer to an vertex.
	 *
	 * May be invalidated on graph change, depending on your
	 * specified vertex storage policy.
	 */
	using vertex_const_descriptor_t = al_traits::const_descriptor_t<GraphConfig, vertex_tag>;

	/*! Type used to refer to an vertex in a non-const way.
	 *
	 * May be invalidated on graph change, depending on your
	 * specified vertex storage policy.
	 */
	using vertex_descriptor_t = al_traits::descriptor_t<GraphConfig, vertex_tag>;

	/*! Type used to refer to an edge.
	 *
	 * May be invalidated on graph change, depending on your
	 * specified edge storage policy.
	 */
	using edge_const_descriptor_t = al_traits::const_descriptor_t<GraphConfig, edge_tag>;

	/*! Type used to refer to an edge in a non-const way.
	 *
	 * May be invalidated on graph change, depending on your
	 * specified edge storage policy.
	 */
	using edge_descriptor_t = al_traits::descriptor_t<GraphConfig, edge_tag>;

	template <typename MapValueType>
	using vertex_map_traits_t = al_traits::descriptor_map_traits_t<GraphConfig, vertex_tag, MapValueType>;

	static constexpr bool is_undirected_v = traits::is_undirected_v<GraphConfig>;

	static constexpr bool is_bidirectional_directed_v = traits::is_bidirectional_directed_v<GraphConfig>;

	static_assert(!(is_undirected_v && is_bidirectional_directed_v),
	              "A graph cannot be both undirected and bidirectional-directed.");

	/*! Create and initialize (if required) a map from
	 * vertex descriptors to MapValueType.
	 */
	template <typename ValueType>
	auto createVertexMap() const
	{
		return vertex_map_traits_t<ValueType>::create(detail::vertexStorage<GraphConfig>(storage_));
	}

	/*! Access edge object for the given edge descriptor.
	 * (const)
	 */
	auto get(edge_const_descriptor_t e) const -> al_traits::element_t<GraphConfig, edge_tag> const&
	{
		return detail::getEdge<GraphConfig>(storage_, e);
	}

private:
	/*! Access edge object for the given edge descriptor.
	 */
	template <typename EdgeDescriptor>
	auto
	getMutable(EdgeDescriptor e,
	           GRAPH_ENABLE_IF_(al_traits::is_correct_descriptor_type_v<GraphConfig, edge_tag, EdgeDescriptor>))
	    -> al_traits::element_t<GraphConfig, edge_tag>&
	{
		if constexpr (EdgeDescriptor::constness == ::cxxgraph::detail::Constness::Const) {
			return detail::getEdge<GraphConfig>(storage_, remove_constness(e));
		} else { // NOLINT
			return detail::getEdge<GraphConfig>(storage_, e);
		}
	}

	/*! Access vertex object for the given vertex descriptor.
	 * (const)
	 */
	auto get(vertex_const_descriptor_t v) const -> al_traits::element_t<GraphConfig, vertex_tag> const&
	{
		return detail::getVertex<GraphConfig>(storage_, v);
	}

	/*! Access vertex object for the given vertex descriptor.
	 */
	template <typename VertexDescriptor>
	auto
	getMutable(VertexDescriptor v,
	           GRAPH_ENABLE_IF_(al_traits::is_correct_descriptor_type_v<GraphConfig, vertex_tag, VertexDescriptor>))
	    -> al_traits::element_t<GraphConfig, vertex_tag>&
	{
		static_assert(al_traits::is_correct_descriptor_type_v<GraphConfig, vertex_tag, VertexDescriptor>);
		if constexpr (VertexDescriptor::constness == ::cxxgraph::detail::Constness::Const) {
			return detail::getVertex<GraphConfig>(storage_, remove_constness(v));

		} else { // NOLINT
			return detail::getVertex<GraphConfig>(storage_, v);
		}
	}

public:
	/*! Read-only access to a range of (edge descriptor, reference to edge)
	 * pairs containing all edges coming in to v (bidirectional directed
	 * graphs only).
	 *
	 * May throw if v is not a valid vertex descriptor.
	 */
	auto vertexInEdges(vertex_const_descriptor_t v) const
	{
		static_assert(is_bidirectional_directed_v,
		              "vertexInEdges() is only permitted on bidirectional directed graphs.");
		return detail::wrapEdgeDescriptorRange<GraphConfig>(
		    storage_, detail::vertexIncomingEdges<GraphConfig>(storage_, v));
	}

	/*! Read-only access to a range of (edge descriptor, reference to edge)
	 * pairs containing all edges going out from v.
	 *
	 * May throw if v is not a valid vertex descriptor.
	 */
	auto vertexOutEdges(vertex_const_descriptor_t v) const
	{
		return detail::vertexOutgoingEdgeRange<GraphConfig>(storage_, v);
	}

	/*! Read-only access to a range of edge descriptors containing all edges
	 * coming in to v (bidirectional directed graphs only).
	 *
	 * May throw if v is not a valid vertex descriptor.
	 */
	auto&& vertexInEdgeDescriptors(vertex_const_descriptor_t v) const
	{
		static_assert(is_bidirectional_directed_v,
		              "vertexInEdgeDescriptors() is only permitted on bidirectional directed graphs.");
		return detail::vertexIncomingEdges<GraphConfig>(storage_, v);
	}

	/*! Read-only access to a range of edge descriptors containing all edges
	 * going out from v.
	 *
	 * May throw if v is not a valid vertex descriptor.
	 */
	auto vertexOutEdgeDescriptors(vertex_const_descriptor_t v) const
	{
		return detail::vertexOutgoingEdgeDescriptors<GraphConfig>(storage_, v);
	}

	/*! Add a new vertex.
	 *
	 * All arguments are forwarded to the VertexProperties
	 * constructor.
	 *
	 * @returns the vertex descriptor associated with the
	 * newly-created vertex.
	 */
	template <typename... Args>
	auto addVertex(Args&&... a) -> vertex_descriptor_t
	{
		return detail::addVertex<GraphConfig>(storage_, std::forward<Args>(a)...);
	}

	/*! Queries if there is a directed edge from source to
	 * target.
	 *
	 * May throw if source or target are not valid vertex
	 * descriptors.
	 */
	bool hasEdge(vertex_const_descriptor_t source, vertex_const_descriptor_t target) const
	{
		return detail::hasEdge<GraphConfig>(storage_, source, target);
	}

	/*! Add an edge from source to target, with remaining
	 * arguments forwarded to the EdgeProperties
	 * constructor.
	 *
	 * Throws if an edge from source to target already
	 * exists. May throw if source or target are not valid
	 * vertex descriptors.
	 *
	 * @returns the edge descriptor associated with the
	 * newly-created edge.
	 */
	template <typename... Args>
	auto addEdge(vertex_const_descriptor_t source, vertex_const_descriptor_t target, Args&&... a)
	    -> edge_descriptor_t
	{
		return detail::addEdge<GraphConfig>(storage_, source, target, std::forward<Args>(a)...);
	}

	/*! Get a read-only range of all edges (as (edge descriptor, const
	 * reference to edge) pairs) in the graph.
	 */
	auto edges() const { return detail::edges<GraphConfig>(storage_); }

	/*! Get a read-only range of all vertices (as (vertex descriptor, const
	 * reference to vertex) pairs) in the graph.
	 */
	auto vertices() const { return vertex_traits_t::getRange(detail::vertexStorage<GraphConfig>(storage_)); }

	/*! Get a read-only range of all edge descriptors in the graph.
	 */
	auto edgeDescriptors() const { return detail::edgeDescriptors<GraphConfig>(storage_); }

	/*! Get a read-only range of all vertex descriptors in the graph.
	 */
	auto vertexDescriptors() const
	{
		return vertex_traits_t::getDescriptors(detail::vertexStorage<GraphConfig>(storage_));
	}

	/*! Get the number of edges in the graph.
	 */
	auto numEdges() const -> std::size_t { return detail::numEdges<GraphConfig>(storage_); }

	/*! Get the number of vertices in the graph.
	 */
	auto numVertices() const -> std::size_t { return detail::numVertices<GraphConfig>(storage_); }

private:
	template <typename VertexDescriptor>
	void normalizeEndpoints(VertexDescriptor& source, VertexDescriptor& target) const
	    noexcept(!is_undirected_v || noexcept(swap(source, target)))
	{
		static_assert(al_traits::is_correct_descriptor_type_v<GraphConfig, vertex_tag, VertexDescriptor>);
		detail::normalizeEndpoints<GraphConfig>(source, target);
	}

	storage_t storage_;

	template <typename Tag>
	static constexpr bool const_descriptor_same_as_descriptor_v =
	    std::is_same<al_traits::const_descriptor_t<GraphConfig, Tag>,
	                 al_traits::descriptor_t<GraphConfig, Tag>>::value;

	static_assert(al_traits::absolute_descriptors_v<GraphConfig, vertex_tag> !=
	                  const_descriptor_same_as_descriptor_v<vertex_tag>,
	              "Vertex storage policy indicates that descriptors are absolute, but descriptor_t and "
	              "const_descriptor_t are the same, which is not valid.");

	static_assert(al_traits::absolute_descriptors_v<GraphConfig, edge_tag> !=
	                  const_descriptor_same_as_descriptor_v<edge_tag>,
	              "Edge storage policy indicates that descriptors are absolute, but descriptor_t and "
	              "const_descriptor_t are the same, which is not valid.");
};

} // namespace cxxgraph::adjacency_list
