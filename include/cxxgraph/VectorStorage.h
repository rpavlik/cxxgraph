// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: Apache-2.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// Internal Includes
#include "Base.h"

// Library Includes
#include <range/v3/core.hpp>
#include <range/v3/view/iota.hpp>
#include <range/v3/view/zip.hpp>

// Standard Includes
#include <cstdint>
#include <limits>
#include <stdexcept>
#include <vector>

namespace cxxgraph::policy {
/*! Element storage policy that stores elements in a std::vector
 * and uses element index as the descriptor.
 */
struct VectorElementStoragePolicy;

//! Struct for stream output of vector indices.
struct VectorIndexPrintable
{
	using contained_type = std::size_t;

	//! Invalid value is the max representable value.
	static constexpr contained_type invalid_contained_value = std::numeric_limits<contained_type>::max();

	//! The wrapped index, defaults to the invalid value.
	contained_type index = invalid_contained_value;
};

//! Wrapper for std::size_t that has a unique "uninitialized" state.
template <typename Tag>
struct VectorIndex : detail::DescriptorBase<VectorIndex<Tag>, Tag>
{

	static_assert(is_edge_or_vertex_tag_v<Tag>, "Tag must be edge_tag or vertex_tag");
	//! Wrapped index number type.
	using contained_type = VectorIndexPrintable::contained_type;
	using difference_type = std::ptrdiff_t;
	//! Invalid value is the max representable value.
	static constexpr contained_type invalid_contained_value = VectorIndexPrintable::invalid_contained_value;

	constexpr VectorIndex() noexcept = default;
	constexpr VectorIndex(contained_type i) noexcept : index(i) {}

	//! The wrapped index, defaults to the invalid value.
	contained_type index = invalid_contained_value;

	//! Called by shared stream output functions.
	constexpr VectorIndexPrintable printable() const noexcept { return {index}; }

	/*! Checks to see if this value is valid, or invalid
	 * (uninitialized, etc)..
	 */
	constexpr explicit operator bool() const noexcept { return index != invalid_contained_value; }

	//! Equality comparison
	constexpr bool operator==(VectorIndex other) const noexcept { return index == other.index; }

	//! Inequality comparison
	constexpr bool operator!=(VectorIndex other) const noexcept { return index != other.index; }

	//! Less-than comparison
	constexpr bool operator<(VectorIndex other) const noexcept { return index < other.index; }

	//! Pre-increment
	constexpr VectorIndex& operator++() noexcept
	{
		++index;
		return *this;
	}

	//! Post-increment
	constexpr VectorIndex operator++(int) noexcept
	{
		VectorIndex ret(*this);
		++index;
		return ret;
	}

	//! Subtraction
	constexpr difference_type operator-(VectorIndex other) const noexcept
	{
		return static_cast<difference_type>(index) - static_cast<difference_type>(other.index);
	}
};

/*! Swap function for VectorIndex.
 * @relates VectorIndex.
 */
template <typename Tag>
inline void swap(VectorIndex<Tag>& a, VectorIndex<Tag>& b) noexcept
{
	using std::swap;
	swap(a.index, b.index);
}

template <typename Tag>
using vector_descriptor_t = VectorIndex<Tag>;

// Traits associated with VectorElementStoragePolicy
template <typename Tag, typename ValueType>
struct ElementStorageTraits<VectorElementStoragePolicy, Tag, ValueType>
{
	using container_t = std::vector<ValueType>;
	using descriptor_t = vector_descriptor_t<Tag>;
	using const_descriptor_t = descriptor_t;

	static constexpr bool absolute_descriptors = false;

	template <typename... Args>
	static descriptor_t emplace(container_t& elements, Args... a)
	{
		auto newDescriptor = elements.size();
		elements.emplace_back(std::forward<Args>(a)...);
		return descriptor_t{newDescriptor};
	}

	static ValueType& at(container_t& elements, descriptor_t d)
	{
		if (!d) {
			throw std::out_of_range("Cannot access element container at "
			                        "invalid descriptor.");
		}
		return elements.at(d.index);
	}

	static ValueType const& at(container_t const& elements, const_descriptor_t d)
	{
		if (!d) {
			throw std::out_of_range("Cannot access element container at "
			                        "invalid descriptor.");
		}
		return elements.at(d.index);
	}

	static auto getRange(container_t const& elements)
	{
		auto count = elements.size();
		return ranges::view::zip(ranges::view::iota(const_descriptor_t(0), const_descriptor_t(count)),
		                         elements);
	}
	static auto getRange(container_t& elements)
	{
		auto count = elements.size();
		return ranges::view::zip(ranges::view::iota(descriptor_t(0), descriptor_t(count)), elements);
	}
	static auto getDescriptors(container_t const& elements)
	{
		auto count = elements.size();
		return ranges::view::iota(const_descriptor_t(0), const_descriptor_t(count));
	}
};

// Descriptor map traits associated with
// VectorElementStoragePolicy - descriptor maps are always
// vectors in this case.
template <typename Tag, typename ValueType, typename MapValueType>
struct DescriptorMapTraits<VectorElementStoragePolicy, Tag, ValueType, MapValueType>
{
	using descriptor_map_t = std::vector<MapValueType>;
	using const_descriptor_t = VectorIndex<Tag>;
	using descriptor_t = VectorIndex<Tag>;
	using element_container_t = container_t<VectorElementStoragePolicy, Tag, ValueType>;
	static descriptor_map_t create(element_container_t const& elements)
	{
		auto ret = descriptor_map_t{};
		ret.resize(elements.size());
		return ret;
	}

	static auto at(descriptor_map_t& map, const_descriptor_t d) -> decltype(map[d.index])
	{
		if (!d) {
			throw std::out_of_range("Cannot access descriptor map value at "
			                        "invalid descriptor.");
		}
		if (map.size() <= d.index) {
			map.resize(d.index + 1);
		}
		return map[d.index];
	}

	static auto at(descriptor_map_t const& map, const_descriptor_t d) -> decltype(map[d.index])
	{
		if (!d) {
			throw std::out_of_range("Cannot access descriptor map value at "
			                        "invalid descriptor.");
		}
		return map.at(d.index);
	}

	static auto getRange(descriptor_map_t& map)
	{
		auto count = map.size();
		return ranges::view::zip(ranges::view::iota(descriptor_t(0), descriptor_t(count)), map);
	}

	static auto getRange(descriptor_map_t const& map)
	{
		auto count = map.size();
		return ranges::view::zip(ranges::view::iota(const_descriptor_t(0), const_descriptor_t(count)), map);
	}
};
} // namespace cxxgraph::policy
