# `cxxgraph`

A generic graph data structure library, for use with C++17.
Algorithms are separate from data structures, and data structures are configurable by policy.
Simplified and clean interface using modern C++ style and recent standards and idioms.
Header-only, with only one required dependency library (also header-only) outside of the standard library.
Useful functionality, despite a not-very-creative name.

Maintained at <https://gitlab.collabora.com/rpavlik/cxxgraph>

## Getting Started

This is a header-only library, so it doesn't technically need to be installed, you can just drop it into your project.

A [CMake][]-based build system is provided for ease of development and for automated testing.

### TL;DR version to get all prerequisites, including optional ones, on Debian-related distros

Replace all the `-7` with whatever version you like.
If you only have access to stretch, you can go as low as `-3.9` -
but `librange-v3-dev` is in `stretch-backports` only,
so if you don't use backports you'll need to just clone that repo.

    sudo apt install cmake cmake-qt-gui librange-v3-dev doxygen graphviz clang-7 llvm-7 clang-tidy-7 clang-format-7

### Prerequisites

You'll need a **C++17-supporting compiler** for development or use.
The goal at the moment is to support **Clang 3.9** and newer, as well as **GCC 7** and newer.
Visual Studio 2017 version 15.8.5 also works (nothing older has been tested).

- An older C++ standard library (such as that found on Debian Stretch from GCC 6.3) is OK.
- The main C++17 feature that is used is `if constexpr` because it simplifies generic code so much.
  Nested namespace declarations (e.g. `namespace cxxgraph::adjacency_list {}`) are also used,
  to reduce the overall indentation depth.
  The rest of the code is more C++14.

You will also need the **[range-v3][] header-only library**.

- If you're vendoring `cxxgraph` you may consider vendoring `range-v3` as well,
  if you aren't building with MSVC (see below).
- If you're on Debian Linux, something like `sudo apt install librange-v3-dev` should work.
  The 0.3.0 version in `stretch-backports` is new enough.
- `range-v3` is also available via [vcpkg](https://github.com/Microsoft/vcpkg),
  recommended for use with MSVC, but also usable with other compilers/platforms now.
  Something like `.\vcpkg install range-v3` will do the job (flip the backslash for non-Windows).
  Note that as of Visual Studio 2017 v15.8.5, this uses [a fork of range-v3 that has been modified][range-v3-fork]
  for MSVC compatibility, something to keep in mind especially if you would be inclined to vendor
  range-v3.

[range-v3]: https://github.com/ericniebler/range-v3
[range-v3-fork]: https://github.com/Microsoft/range-v3-VS2015

#### Optional and Test Prerequisites

To build the tests and an IDE project, you'll need **[CMake 3.7][CMake]** or newer.

- On Debian and relatives, `sudo apt install cmake` will work.
  You may also want `sudo apt install cmake-qt-gui`.

HTML documentation is generated with Doxygen (and graphviz) if you have it,
by building the `doc` target.

- On Debian and relatives, `sudo apt install doxygen graphviz` installs those.

Additionally, [clang-format][] and [clang-tidy][] are highly recommended if you want to contribute.

- On Debian and relatives, `sudo apt install clang-format clang-tidy` will get you whatever version is default.
- However, I'd recommend using the latest version you have access to,
  since newer version improve standards support and features.
  On Debian-related distros, append something like `-6.0`, `-7`, `-8` to the two package names above
  to choose a particular version.
- See also the [LLVM apt repos](http://apt.llvm.org/) for nightly packages for Debian and Ubuntu.

Code coverage reports are available if building in "Debug" mode with Clang,
and if you have the right additional related LLVM tools (`llvm-cov` and `llvm-profdata`).
`c++filt` is optional but highly recommended if you're going to look at coverage reports.
On Debian and friends, the two LLVM tools are in the
the `llvm`, `llvm-6.0`, `llvm-7`, `llvm-8`, etc. packages,
Additionally, `c++filt` is in the `binutils` Debian package which is required by Clang,
so you probably already have that.

Finally, the tests are built with the [Catch2][] test framework.
A copy of the single-header-include is vendored into this source tree,
but if you would rather use a different (preferably newer) version or
your copy of the `cxxgraph` source tree lacks this file for whatever reason,
the build does search for this on your system first.
It first tries looking for a CMake config file,
such as the one `vcpkg` installs (with `vcpkg install catch2`),
and if that fails, it will look for a `catch2/catch.hpp` header file.
The vendored copy is provided as a fall-back location for this header,
so if it finds a system one it will prefer that unless told otherwise by
setting `CATCH2_INCLUDE_DIR` in CMake.

[CMake]: https://cmake.org
[Doxygen]: http://www.doxygen.nl/
[clang-format]: https://releases.llvm.org/7.0.0/tools/clang/docs/ClangFormat.html
[clang-tidy]: http://releases.llvm.org/7.0.0/tools/clang/tools/extra/docs/clang-tidy/index.html

### Building Tests/Installing

As noted above, installation of this library isn't strictly necessary since it's header-only,
but if you want to, there is an "install target" that will essentially just copy the header files
to an appropriate structure.

> Notes:
>
> - These instructions assume *nix, but Windows should be fairly similar, especially with Powershell.
> - You are certainly welcome to use the CMake GUI instead of the command line,
>   or a different ["project generator"][cmake-generators] for CMake
>   (like `"Ninja"` instead of `"Unix Makefiles"`, or possibly even `"Visual Studio 15 2017"`).
>   It was just quicker to write out command line instructions. :D
> - The project hasn't been tested in directories containing spaces in their name,
>   so to make life easier, just avoid such directories, as you might already do.
>   The CMake scripts probably would need some quotes added to handle this.

Get the project:

    git clone https://gitlab.collabora.com/rpavlik/cxxgraph.git

Switch into the project directory, and make a build directory:

    cd cxxgraph
    mkdir build

> Note: `build/` as well as any directory starting with `build-` will be ignored by git,
> so that naming convention is recommended.
> (That is, `/build/` and `/build-*/` are already in the `.gitignore` file.)

Then, invoke [CMake to generate a project][cmake-generate].
This command assumes that you need to specify your compiler to get a new-enough one,
and that you're using Clang 7 installed in a typical Linux location.
Change the path as required, or if your system's default compiler is sufficiently recent
(or you're using a Visual Studio generator, etc),
just drop the `-DCMAKE_CXX_COMPILER=...` argument.
Additionally, feel free to change the build type or generator ("Ninja" is fast and parallel) as you see fit.
(Omit build type for "multi-configuration generators" like Visual Studio or XCode.)

    cmake .. -DCMAKE_BUILD_TYPE=Debug -DCMAKE_CXX_COMPILER=/usr/bin/clang++-7 -G "Unix Makefiles"

If you plan to install the headers system-wide,
append something like `-DCMAKE_INSTALL_PREFIX=/usr/local` to specify the root of the install directory.

If you're using `vcpkg`, add an argument like `-DCMAKE_TOOLCHAIN_FILE=D:\src\vcpkg\scripts\buildsystems\vcpkg.cmake`
so CMake can find Range-v3 (and optionally your local install of catch2).

To build, [the generic CMake build commands][cmake-build] below will work on all systems,
though you can manually invoke your build tool (`make`, `ninja`, etc.) if you prefer.
The first command builds the tests, the second builds the docs (requires Doxygen),
and the third installs the headers under `${CMAKE_INSTALL_PREFIX}/include/cxxgraph`.
(If using a "multi-configuration generators" like Visual Studio or XCode,
add something like `--config Debug` on the end of each of these commands)

    cmake --build .
    cmake --build . --target doc
    cmake --build . --target install

Alternately, if using Make, the following will build the tests, then build the docs, then install headers.
Replace `make` with `ninja` if you used the Ninja generator.

    make
    make doc
    make install

Documentation can be browsed by opening `docs/html/index.html` in the build directory in a web browser.

[cmake-generate]: https://cmake.org/cmake/help/v3.12/manual/cmake.1.html
[cmake-build]: https://cmake.org/cmake/help/v3.12/manual/cmake.1.html#build-tool-mode

## Running the tests

All tests are built into a single executable,
located at `bin/graph-tests` in your build directory.
(Multi-configuration generators like Visual Studio might insert
a config-specific directory in the middle of that path.)
The tests are written using the excellent [Catch2][] framework and test runner:
see those docs for details on manually running individual tests, etc.
Tests are also enumerated (in `tests/CMakeLists.txt`) for use with [CTest][],
a test-driver tool that comes with CMake.

[Catch2]: https://github.com/catchorg/Catch2
[CTest]: https://cmake.org/cmake/help/v3.12/manual/ctest.1.html

> Note: All commands below assume they're being run from the build directory.

**To run all tests via CTest**, run the following:

    ctest

which will produce something like this for output:

```
Test project /home/ryan/src/cxxgraph/build
    Start 1: Iterators_enumerate
1/9 Test #1: Iterators_enumerate ..............................................   Passed    0.00 sec
    Start 2: AdjacencyListGraph_undir-string-string-graph
2/9 Test #2: AdjacencyListGraph_undir-string-string-graph .....................   Passed    0.00 sec
    Start 3: AdjacencyListGraph_string-string-graph
3/9 Test #3: AdjacencyListGraph_string-string-graph ...........................   Passed    0.00 sec
    Start 4: AdjacencyListGraph_bidirectional-string-string-graph
4/9 Test #4: AdjacencyListGraph_bidirectional-string-string-graph .............   Passed    0.00 sec
    Start 5: AdjacencyListGraph_vertex-list-string-string-graph
5/9 Test #5: AdjacencyListGraph_vertex-list-string-string-graph ...............   Passed    0.00 sec
    Start 6: AdjacencyListGraph_edge-list-undir-string-string-graph
6/9 Test #6: AdjacencyListGraph_edge-list-undir-string-string-graph ...........   Passed    0.00 sec
    Start 7: AdjacencyListGraph_edge-list-string-string-graph
7/9 Test #7: AdjacencyListGraph_edge-list-string-string-graph .................   Passed    0.00 sec
    Start 8: AdjacencyListGraph_edge-list-bidirectional-string-string-graph
8/9 Test #8: AdjacencyListGraph_edge-list-bidirectional-string-string-graph ...   Passed    0.00 sec
    Start 9: AdjacencyListGraph_graph
9/9 Test #9: AdjacencyListGraph_graph .........................................   Passed    0.00 sec

100% tests passed, 0 tests failed out of 9

Total Test time (real) =   0.03 sec
```

YOu can also manually run the test executable without having CTest wrap it,
which provides different output as well as access to the various options offered
by Catch2. To do this, run:

    bin/graph-tests

which will produce (possibly colored) output like this:

```
===============================================================================
All tests passed (598 assertions in 9 test cases)
```

Finally, to run tests and process code coverage data into a report
(only available with Clang and building in Debug mode),
you just need to build the `coverage-tests` target, either by

    cmake --build . --target coverage-tests

or something like this (replacing `make` if appropriate):

    make coverage-tests

which will produce console output like this:

```
[0/3] Running graph-tests with coverage enabled
===============================================================================
All tests passed (598 assertions in 9 test cases)

[3/3] Generating coverage report for graph-tests - view /home/ryan/src/cxxgraph/build/tests/coverage-tests/index.html to see results
```

Open the coverage report with a web browser.
Its location should be output as a build message,
but is expected to be under your build directory,
in `tests/coverage-tests/index.html`.

### About tests

In general, there is one `.cpp` file in `tests/` per header that can be individually tested.

## Coding style and formatting

[clang-format][] and [clang-tidy][] are used,
and `.clang-format` and `.clang-tidy` config files are present in the repo
to allow your editor to use them.

To enable clang-tidy being run automatically on build,
enable the CMake option `BUILD_WITH_CLANG_TIDY`.

To manually apply clang-format to every source file in the tree
(that isn't vendored from another project),
run this command in the source dir with a `sh`-compatible shell
(Git for Windows git-bash should be OK):

    ./format-project.sh

You can optionally put something like `CLANG_FORMAT=clang-format-7` before that command
if your clang-format binary isn't named `clang-format`.
Note that you'll typically prefer to use something like `git clang-format`
to just re-format your changes, in case version differences in tools result in overall format changes.

## Built With

- [range-v3][] for transforming, adapting, combining, and generating "ranges"
- [meta](https://ericniebler.github.io/meta/index.html) (developed and bundled with `range-v3`)
  tiny, alias-centric C++11+ metaprogramming library
- [Catch2][] C++ test framework
- [CMake][] for build system/project/Makefile generation.
- [Doxygen][] for documentation generation from extracted code comments.

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md)
for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

This is a young project intended mainly for "vendoring" usage models, so no real versions yet.
Ideally, once it has been tested more wideless, we should use [SemVer](http://semver.org/) for versioning.

## Primary Authors and Contributors

* **Ryan Pavlik** - *Initial work* - @rpavlik on many platforms

## License

The bulk of this project (including all source headers)
is licensed under the Boost Software License v1.0 -
see the [LICENSE.txt](LICENSE.txt) file for details.
A handful of script/tooling files are reused from other projects,
and thus carry the license they were originally released with
(all are permissive free software licenses).
Most files in `vendor/` are from third-party projects and are subject to their respective licenses,
as is the code in any dependencies.
Reasonable efforts have been made to ensure license compatibility and broad usability,
but I am not a lawyer, so be mindful of licenses in this library, its dependencies,
and any other packages you might use as you develop your own software.

All files in this project, except for those copied directly from other projects and placed in `vendor/`,
shall have `SPDX-License-Identifier:` tags as well as a copyright statement.

## Acknowledgments

- [Collabora](https://collabora.com) for supporting Ryan's development and maintenance of this code
  in the course of his work.
- Thanks and acknowledgements to the authors and contributors of the [Boost.Graph][] library,
  for introducing such an important codebase
  and contributing to how the language has evolved and how we write C++ today,
  and whose design decisions informed and inspired choices made for this library.
- Thanks to PurpleBooth's excellent template/advice for structuring a
  [README](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)

[Boost.Graph]: https://www.boost.org/doc/libs/1_68_0/libs/graph/doc/index.html

---

# Copyright and License for this README.md file

For this file only:

> Initially written by Ryan Pavlik. Copyright 2018 Collabora, Ltd.
>
> SPDX-License-Identifier: CC-BY-4.0
