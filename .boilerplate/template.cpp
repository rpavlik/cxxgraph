{%- extends 'base.cpp' -%}
{%- macro fn() -%}{{stem}}.cpp{%- endmacro -%}
{%- block brief %}Implementation{% endblock -%}
{%- block content %}
// Internal Includes
#include "{{stem}}.h"

// Library Includes
// - none

// Standard Includes
// - none

namespace cxxgraph {

} // namespace cxxgraph
{% endblock %}
