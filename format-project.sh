#!/bin/bash
# Copyright 2018, Collabora, Ltd.
# Copyright 2016, Sensics, Inc.
# SPDX-License-Identifier: Apache-2.0

CLANG_FORMAT=${CLANG_FORMAT:-clang-format}
runClangFormatOnDir() {
    find "$1" \( -name "*.c" -o -name "*.cpp" -o -name "*.c" -o -name "*.h" \)| \
        grep -v "\.boilerplate" | \
        xargs ${CLANG_FORMAT} -style=file -i
}

(
cd $(dirname $0)
runClangFormatOnDir include
runClangFormatOnDir tests
)
