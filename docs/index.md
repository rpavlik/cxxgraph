
# Main Page {#mainpage}

This is a modern C++ generic graph (vertex-and-edge, not charting) library.
Some design decisions inspired by Boost Graph, but simpler and a more modern style.
