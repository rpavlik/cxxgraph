// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// Internal Includes
// - none

// Library Includes
#include <catch2/catch.hpp>

// Standard Includes
#include <initializer_list>
#include <sstream>

template <typename T>
class InSetMatcher : public Catch::MatcherBase<T>
{
public:
	InSetMatcher(std::initializer_list<T> const& s) : s_(s) {}
	bool match(T const& d) const override { return std::find(s_.begin(), s_.end(), d) != s_.end(); }
	std::string describe() const override
	{
		std::ostringstream ss;
		ss << "is one of {";

		for (auto& acceptable : s_) {
			ss << acceptable << ", ";
		}
		ss << "}";
		return ss.str();
	}

private:
	std::initializer_list<T> s_;
};

template <typename T>
inline auto IsInSet(std::initializer_list<T> const& s)
{
	return InSetMatcher<T>{s};
}
