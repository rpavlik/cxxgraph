// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Tests for cxxgraph/ConfigMaker.h
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

// Internal Includes
#include "cxxgraph/ConfigMaker.h"

// Library Includes
#include <meta/meta.hpp>

// Standard Includes
#include <type_traits>

using std::is_same;

using namespace cxxgraph::config;
using namespace cxxgraph::config::detail;
namespace m = meta;

using T1 = int;
using T2 = bool;
#define TYPES T1, T2
using tlist = m::list<TYPES>;

#define TYPES2 VertexStorage<int>, VertexProperties<int>, EdgeStorage<int>, EdgeProperties<int>
using tlist2 = m::list<TYPES2>;

// Check GetConfigs to make sure it's creating the right config list for each type of argument.
static_assert(is_same<GetConfigs<T1>, m::list<T1>>());
static_assert(is_same<GetConfigs<tlist>, tlist>());
static_assert(is_same<GetConfigs<config_maker_t<TYPES2>>, m::list<TYPES2>>());

// Check ConcatConfigLists for a variety of input.
static_assert(is_same<ConcatConfigLists<TYPES>, tlist>());
static_assert(is_same<m::front<ConcatConfigLists<TYPES>>, T1>());
static_assert(is_same<m::back<ConcatConfigLists<TYPES>>, T2>());
static_assert(is_same<ConcatConfigLists<tlist>, tlist>());

static_assert(is_same<ConcatConfigLists<TYPES2>, tlist2>());
static_assert(is_same<m::front<ConcatConfigLists<TYPES2>>, VertexStorage<int>>());
static_assert(is_same<m::back<ConcatConfigLists<TYPES2>>, EdgeProperties<int>>());
static_assert(is_same<ConcatConfigLists<tlist2>, tlist2>());
static_assert(is_same<ConcatConfigLists<config_maker_t<TYPES2>>, tlist2>());

static_assert(is_same<ConcatConfigLists<config_maker_t<TYPES2>, tlist>, m::concat<tlist2, tlist>>());

// Check the counting used for verifying required conditions.
static_assert(m::size<filter_derived_from_t<m::list<Undirected, TYPES>, DirectednessBase>>() == 1);
static_assert(m::size<filter_derived_from_t<m::list<BidirectionalDirected, TYPES>, DirectednessBase>>() == 1);
static_assert(m::size<filter_derived_from_t<m::list<UnidirectionalDirected, TYPES>, DirectednessBase>>() == 1);
static_assert(m::size<filter_derived_from_t<m::list<EdgeProperties<int>, TYPES>, EdgePropertiesBase>>() == 1);
static_assert(m::size<filter_derived_from_t<m::list<VertexProperties<int>, TYPES>, VertexPropertiesBase>>() == 1);
static_assert(m::size<filter_derived_from_t<m::list<VertexStorage<int>, TYPES>, VertexStorageBase>>() == 1);
static_assert(m::size<filter_derived_from_t<m::list<EdgeStorage<int>, TYPES>, EdgeStorageBase>>() == 1);
static_assert(m::size<filter_derived_from_t<
                  m::list<EdgeStorage<int>, VertexStorage<int>, VertexProperties<int>, EdgeProperties<int>, TYPES>,
                  ConfigMakerElementBase>>() == 4);
