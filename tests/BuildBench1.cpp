// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Dummy file to profile template instantiation.
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

// Internal Includes
#include "cxxgraph/AdjacencyListGraph.h"
#include "cxxgraph/ConfigMaker.h"
#include "cxxgraph/VectorStorage.h"

// Library Includes
// - none

// Standard Includes
#include <string>

namespace my_config {
using namespace cxxgraph::config;
using cxxgraph::policy::VectorElementStoragePolicy;

// All configs store strings for edge and vertex properties
using StringString = meta::list<EdgeProperties<std::string>, VertexProperties<std::string>>;

// Vertices stored in vector, Edges stored in vector
using VecVecStringStringConfig =
    config_maker_t<StringString, VertexStorage<VectorElementStoragePolicy>, EdgeStorage<VectorElementStoragePolicy>>;

} // namespace my_config
using GraphStorage = cxxgraph::adjacency_list::detail::storage_t<my_config::VecVecStringStringConfig>;
// using Graph = cxxgraph::adjacency_list::Graph<my_config::VecVecStringStringConfig>;
void createGraph() { GraphStorage g; }
