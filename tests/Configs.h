// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// Internal Includes
#include "cxxgraph/ConfigMaker.h"
#include "cxxgraph/ListStorage.h"
#include "cxxgraph/VectorStorage.h"

// Library Includes
// - none

// Standard Includes
// - none

namespace my_config {
using namespace cxxgraph::config;
using cxxgraph::policy::ListElementStoragePolicy;
using cxxgraph::policy::VectorElementStoragePolicy;

// All configs store strings for edge and vertex properties
using StringString = meta::list<EdgeProperties<std::string>, VertexProperties<std::string>>;

// Vertices stored in vector, Edges stored in vector
using VecVecStringStringConfig =
    config_maker_t<StringString, VertexStorage<VectorElementStoragePolicy>, EdgeStorage<VectorElementStoragePolicy>>;
using BidiVecVecStringStringConfig = config_maker_t<BidirectionalDirected, VecVecStringStringConfig>;
using UndirVecVecStringStringConfig = config_maker_t<Undirected, VecVecStringStringConfig>;

// Vertices stored in list, Edges stored in vector
// Tests out list policy without the added complexity of permitting "internalized edges"
using ListVecStringStringConfig =
    config_maker_t<StringString, EdgeStorage<VectorElementStoragePolicy>, VertexStorage<ListElementStoragePolicy>>;

// Vertices stored in vector, Edges stored in lists
// (These generally permit "internalizing" edges since the edge descriptors are wrapped pointers)
using VecListStringStringConfig =
    config_maker_t<StringString, VertexStorage<VectorElementStoragePolicy>, EdgeStorage<ListElementStoragePolicy>>;
using BidiVecListStringStringConfig = config_maker_t<BidirectionalDirected, VecListStringStringConfig>;
using UndirVecListStringStringConfig = config_maker_t<Undirected, VecListStringStringConfig>;

static_assert(!cxxgraph::traits::is_bidirectional_directed_v<VecVecStringStringConfig>);
static_assert(!cxxgraph::traits::is_undirected_v<VecVecStringStringConfig>);

static_assert(cxxgraph::traits::is_bidirectional_directed_v<BidiVecVecStringStringConfig>);
static_assert(!cxxgraph::traits::is_undirected_v<BidiVecVecStringStringConfig>);

static_assert(!cxxgraph::traits::is_bidirectional_directed_v<UndirVecVecStringStringConfig>);
static_assert(cxxgraph::traits::is_undirected_v<UndirVecVecStringStringConfig>);

static_assert(!cxxgraph::traits::is_bidirectional_directed_v<ListVecStringStringConfig>);
static_assert(!cxxgraph::traits::is_undirected_v<ListVecStringStringConfig>);

static_assert(!cxxgraph::traits::is_bidirectional_directed_v<VecListStringStringConfig>);
static_assert(!cxxgraph::traits::is_undirected_v<VecListStringStringConfig>);

static_assert(cxxgraph::traits::is_bidirectional_directed_v<BidiVecListStringStringConfig>);
static_assert(!cxxgraph::traits::is_undirected_v<BidiVecListStringStringConfig>);

static_assert(!cxxgraph::traits::is_bidirectional_directed_v<UndirVecListStringStringConfig>);
static_assert(cxxgraph::traits::is_undirected_v<UndirVecListStringStringConfig>);
} // namespace my_config
