// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Tests for cxxgraph/AdjacencyListGraph.h
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

// Internal Includes
#include "cxxgraph/AdjacencyListGraph.h"
#include "Configs.h"
#include "cxxgraph/Stream.h"

// Library Includes
#include <catch2/catch.hpp>

// Standard Includes
#include <set>
#include <string>

using namespace cxxgraph;
namespace {
template <typename I>
struct CheckIter
{
	static_assert(ranges::Iterator<I>());
	static_assert(ranges::Readable<I>());
	static_assert(ranges::Iterator<I>());
	static_assert(ranges::ForwardIterator<I>());
};

template <typename Rng>
struct CheckRange
{
	static_assert(ranges::Range<Rng>());
	static_assert(ranges::InputRange<Rng>());
	static_assert(ranges::ForwardRange<Rng>());
	using check_iter = CheckIter<decltype(std::declval<Rng>().begin())>;
};

template <typename Policy>
struct CheckPolicy
{
	using value_type = int;
	using const_ref = value_type const&;
	using ref = value_type&;
	using tag = edge_tag;
	using storage_traits_t = policy::ElementStorageTraits<Policy, tag, value_type>;
	using container_t = typename storage_traits_t::container_t;
	using descriptor_t = typename storage_traits_t::descriptor_t;
	using const_descriptor_t = typename storage_traits_t::const_descriptor_t;

	static_assert(
	    !std::is_void<decltype(std::declval<const_descriptor_t&>() = std::declval<const_descriptor_t const&>())>(),
	    "const_descriptor_t needs to be assignable");
	static_assert(
	    !std::is_void<decltype(std::declval<const_descriptor_t&>() = std::declval<descriptor_t const&>())>(),
	    "descriptor_t needs to be implicitly convertible to const_descriptor_t");
	static_assert(const_descriptor_t::constness == detail::Constness::Const ||
	              const_descriptor_t::constness == detail::Constness::NotConstIndicating);
	static_assert(descriptor_t::constness == detail::Constness::NonConst ||
	              descriptor_t::constness == detail::Constness::NotConstIndicating);

	static_assert(!std::is_void<decltype(storage_traits_t::getRange(std::declval<container_t const&>()))>());
	static_assert(!std::is_void<decltype(storage_traits_t::getRange(std::declval<container_t&>()))>());
	static_assert(!std::is_void<decltype(storage_traits_t::getDescriptors(std::declval<container_t const&>()))>());

	static_assert(std::is_same<const_ref, decltype(storage_traits_t::at(std::declval<container_t const&>(),
	                                                                    std::declval<const_descriptor_t>()))>());
	static_assert(std::is_same<const_ref, decltype(storage_traits_t::at(std::declval<container_t const&>(),
	                                                                    std::declval<descriptor_t>()))>());
	static_assert(std::is_same<ref, decltype(storage_traits_t::at(std::declval<container_t&>(),
	                                                              std::declval<descriptor_t>()))>());
	using check_const_getRange =
	    CheckRange<decltype(storage_traits_t::getRange(std::declval<container_t const&>()))>;
	using check_getRange = CheckRange<decltype(storage_traits_t::getRange(std::declval<container_t&>()))>;
	using check_getDescriptors =
	    CheckRange<decltype(storage_traits_t::getDescriptors(std::declval<container_t const&>()))>;
};
using policy_checking = meta::list<                  //
    CheckPolicy<policy::VectorElementStoragePolicy>, //
    CheckPolicy<policy::ListElementStoragePolicy>>;
/*

Graph:

a  ->  b

|      |
v      v

c  ->  d

*/
template <typename GraphConfig>
struct TestFixture
{

	using Graph = adjacency_list::Graph<GraphConfig>;
	using vertex_const_descriptor_t = graph_vertex_const_descriptor_t<Graph>;
	using edge_const_descriptor_t = graph_edge_const_descriptor_t<Graph>;

	void addVertices()
	{
		REQUIRE(0 == g.numVertices());

		a = g.addVertex("a");
		REQUIRE_FALSE(a == vertex_const_descriptor_t{});
		REQUIRE(1 == g.numVertices());
		REQUIRE(g.vertexDescriptors().front() == a);
		REQUIRE(g.getProperties(a) == "a");
		REQUIRE(vertices.find(a) == vertices.end());
		vertices.insert(a);

		b = g.addVertex("b");
		REQUIRE_FALSE(b == vertex_const_descriptor_t{});
		REQUIRE(2 == g.numVertices());
		REQUIRE(g.getProperties(b) == "b");
		REQUIRE(vertices.find(b) == vertices.end());
		vertices.insert(b);

		c = g.addVertex("c");
		REQUIRE_FALSE(c == vertex_const_descriptor_t{});
		REQUIRE(3 == g.numVertices());
		REQUIRE(g.getProperties(c) == "c");
		REQUIRE(vertices.find(c) == vertices.end());
		vertices.insert(c);

		d = g.addVertex("d");
		REQUIRE_FALSE(d == vertex_const_descriptor_t{});
		REQUIRE(4 == g.numVertices());
		REQUIRE(g.getProperties(d) == "d");
		REQUIRE(vertices.find(d) == vertices.end());
		vertices.insert(d);
	}
	void addEdges()
	{

		REQUIRE(0 == g.numEdges());
		REQUIRE_FALSE(g.hasEdge(a, b));
		REQUIRE_FALSE(g.hasEdge(b, a));
		REQUIRE_FALSE(g.hasEdge(a, c));
		REQUIRE_FALSE(g.hasEdge(c, a));
		REQUIRE_FALSE(g.hasEdge(c, d));
		REQUIRE_FALSE(g.hasEdge(d, c));
		REQUIRE_FALSE(g.hasEdge(b, d));
		REQUIRE_FALSE(g.hasEdge(d, b));

		ab = g.addEdge(a, b, "ab");
		REQUIRE_FALSE(ab == edge_const_descriptor_t{});
		REQUIRE(1 == g.numEdges());
		REQUIRE(g.edgeDescriptors().front() == ab);
		REQUIRE(g.get(ab).connectivity.source == a);
		REQUIRE(g.get(ab).connectivity.target == b);
		REQUIRE(g.getSource(ab) == a);
		REQUIRE(g.getTarget(ab) == b);
		REQUIRE(g.hasEdge(a, b));
		REQUIRE(edges.find(ab) == edges.end());
		edges.insert(ab);

		ac = g.addEdge(a, c, "ac");
		REQUIRE_FALSE(ac == edge_const_descriptor_t{});
		REQUIRE(2 == g.numEdges());
		REQUIRE(g.get(ac).connectivity.source == a);
		REQUIRE(g.get(ac).connectivity.target == c);
		REQUIRE(g.hasEdge(a, c));
		REQUIRE(edges.find(ac) == edges.end());
		edges.insert(ac);

		cd = g.addEdge(c, d, "cd");
		REQUIRE_FALSE(cd == edge_const_descriptor_t{});
		REQUIRE(3 == g.numEdges());
		REQUIRE(g.get(cd).connectivity.source == c);
		REQUIRE(g.get(cd).connectivity.target == d);
		REQUIRE(g.hasEdge(c, d));
		REQUIRE(edges.find(cd) == edges.end());
		edges.insert(cd);

		bd = g.addEdge(b, d, "bd");
		REQUIRE_FALSE(bd == edge_const_descriptor_t{});
		REQUIRE(4 == g.numEdges());
		REQUIRE(g.get(bd).connectivity.source == b);
		REQUIRE(g.get(bd).connectivity.target == d);
		REQUIRE(g.hasEdge(b, d));
		REQUIRE(edges.find(bd) == edges.end());
		edges.insert(bd);

		REQUIRE_THROWS(g.get(edge_const_descriptor_t{}));

		REQUIRE_THROWS(g.vertexOutEdges(vertex_const_descriptor_t{}));
		{
			// Force these templates to be instantiated to run static_assert on the types.
			static_assert(!std::is_void<CheckRange<decltype(g.edgeDescriptors())>>());
			static_assert(!std::is_void<CheckRange<decltype(g.edges())>>());
			static_assert(!std::is_void<CheckRange<decltype(g.vertexDescriptors())>>());
			static_assert(!std::is_void<CheckRange<decltype(g.vertices())>>());
		}
	}

	Graph g;
	vertex_const_descriptor_t a;
	vertex_const_descriptor_t b;
	vertex_const_descriptor_t c;
	vertex_const_descriptor_t d;
	std::set<vertex_const_descriptor_t> vertices;

	edge_const_descriptor_t ab;
	edge_const_descriptor_t ac;
	edge_const_descriptor_t cd;
	edge_const_descriptor_t bd;
	std::set<edge_const_descriptor_t> edges;
};
} // namespace

TEST_CASE("graph")
{
	using Graph = adjacency_list::Graph<my_config::VecVecStringStringConfig>;
	Graph g;
	REQUIRE(0 == g.numVertices());

	auto a = g.addVertex("a");
	REQUIRE(1 == g.numVertices());
	REQUIRE(g.vertexDescriptors().front() == a);
	REQUIRE(g.getProperties(a) == "a");

	auto b = g.addVertex("b");
	REQUIRE(2 == g.numVertices());
	REQUIRE(g.getProperties(b) == "b");

	auto c = g.addVertex("c");
	REQUIRE(3 == g.numVertices());
	REQUIRE(g.getProperties(c) == "c");

	auto d = g.addVertex("d");
	REQUIRE(4 == g.numVertices());
	REQUIRE(g.getProperties(d) == "d");

	REQUIRE_FALSE(g.hasEdge(a, b));
	REQUIRE_FALSE(g.hasEdge(b, a));
	REQUIRE_FALSE(g.hasEdge(a, c));
	REQUIRE_FALSE(g.hasEdge(c, a));
	REQUIRE_FALSE(g.hasEdge(c, d));
	REQUIRE_FALSE(g.hasEdge(d, c));
	REQUIRE_FALSE(g.hasEdge(b, d));
	REQUIRE_FALSE(g.hasEdge(d, b));

	REQUIRE(0 == g.numEdges());

	auto ab = g.addEdge(a, b, "ab");
	REQUIRE(1 == g.numEdges());
	REQUIRE(g.edgeDescriptors().front() == ab);
	REQUIRE(getSource(g.edges().front()) == a);
	REQUIRE(getTarget(g.edges().front()) == b);
	REQUIRE(g.get(ab).connectivity.source == a);
	REQUIRE(g.get(ab).connectivity.target == b);
	REQUIRE(g.hasEdge(a, b));

	auto ac = g.addEdge(a, c, "ac");
	REQUIRE(2 == g.numEdges());
	REQUIRE(g.get(ac).connectivity.source == a);
	REQUIRE(g.get(ac).connectivity.target == c);
	REQUIRE(g.hasEdge(a, c));

	auto cd = g.addEdge(c, d, "cd");
	REQUIRE(3 == g.numEdges());
	REQUIRE(g.get(cd).connectivity.source == c);
	REQUIRE(g.get(cd).connectivity.target == d);
	REQUIRE(g.hasEdge(c, d));

	auto bd = g.addEdge(b, d, "bd");
	REQUIRE(4 == g.numEdges());
	REQUIRE(g.get(bd).connectivity.source == b);
	REQUIRE(g.get(bd).connectivity.target == d);
	REQUIRE(g.hasEdge(b, d));
}

TEST_CASE_METHOD(TestFixture<my_config::VecVecStringStringConfig>, "VecVecStringString")
{
	addVertices();
	addEdges();
	REQUIRE_FALSE(g.hasEdge(b, a));
	REQUIRE_FALSE(g.hasEdge(c, a));
	REQUIRE_FALSE(g.hasEdge(d, c));
	REQUIRE_FALSE(g.hasEdge(d, b));
}
TEST_CASE_METHOD(TestFixture<my_config::BidiVecVecStringStringConfig>, "BidiVecVecStringString")
{
	addVertices();
	addEdges();
	REQUIRE_FALSE(g.hasEdge(b, a));
	REQUIRE_FALSE(g.hasEdge(c, a));
	REQUIRE_FALSE(g.hasEdge(d, c));
	REQUIRE_FALSE(g.hasEdge(d, b));
}

TEST_CASE_METHOD(TestFixture<my_config::UndirVecVecStringStringConfig>, "UndirVecVecStringString")
{
	addVertices();
	addEdges();
	REQUIRE(g.hasEdge(b, a));
	REQUIRE(g.hasEdge(c, a));
	REQUIRE(g.hasEdge(d, c));
	REQUIRE(g.hasEdge(d, b));
	WHEN("adding to undirected with out-of-order endpoints")
	{
		auto da = g.addEdge(d, a, "da");
		THEN("Addition should succeed")
		{

			REQUIRE(g.hasEdge(d, a));
			REQUIRE(g.hasEdge(a, d));
		}
		THEN("Order should be normalized")
		{

			REQUIRE(g.get(da).connectivity.source == a);
			REQUIRE(g.get(da).connectivity.target == d);
		}
	}
}
TEST_CASE_METHOD(TestFixture<my_config::ListVecStringStringConfig>, "ListVecStringString")
{
	addVertices();
	addEdges();
}

TEST_CASE_METHOD(TestFixture<my_config::VecListStringStringConfig>, "VecListStringString")
{
	addVertices();
	addEdges();
	REQUIRE_FALSE(g.hasEdge(b, a));
	REQUIRE_FALSE(g.hasEdge(c, a));
	REQUIRE_FALSE(g.hasEdge(d, c));
	REQUIRE_FALSE(g.hasEdge(d, b));
}

TEST_CASE_METHOD(TestFixture<my_config::BidiVecListStringStringConfig>, "BidiVecListStringString")
{
	addVertices();
	addEdges();
	REQUIRE_FALSE(g.hasEdge(b, a));
	REQUIRE_FALSE(g.hasEdge(c, a));
	REQUIRE_FALSE(g.hasEdge(d, c));
	REQUIRE_FALSE(g.hasEdge(d, b));
}

TEST_CASE_METHOD(TestFixture<my_config::UndirVecListStringStringConfig>, "UndirVecListStringString")
{
	addVertices();
	addEdges();
	REQUIRE(g.hasEdge(b, a));
	REQUIRE(g.hasEdge(c, a));
	REQUIRE(g.hasEdge(d, c));
	REQUIRE(g.hasEdge(d, b));
	WHEN("adding to undirected with out-of-order endpoints")
	{
		auto da = g.addEdge(d, a, "da");
		THEN("Addition should succeed")
		{

			REQUIRE(g.hasEdge(d, a));
			REQUIRE(g.hasEdge(a, d));
		}
		THEN("Order should be normalized")
		{

			REQUIRE(g.get(da).connectivity.source == a);
			REQUIRE(g.get(da).connectivity.target == d);
		}
	}
}
