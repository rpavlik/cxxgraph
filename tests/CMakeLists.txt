# Copyright (c) 2018 Collabora, Ltd.
#
# SPDX-License-Identifier: BSL-1.0

find_package(Threads REQUIRED)
include(CTest)
enable_testing()

# Compile the "main()" provided by catch separately
set(TEST_TARGET "graph-tests")
add_executable(${TEST_TARGET} RunnerStub.cpp Configs.h Matcher.h)
target_link_libraries(${TEST_TARGET}
    PUBLIC
    ${PROJECT_NAME}
    graph-catch2)
target_compile_definitions(${TEST_TARGET}
    PUBLIC
    CATCH_CONFIG_FAST_COMPILE)
if(NOT WIN32)
    target_compile_definitions(${TEST_TARGET}
        PUBLIC
        CATCH_CONFIG_POSIX_SIGNALS)
endif()
if(BUILD_COVERAGE)
    graph_add_coverage(coverage-tests ${TEST_TARGET})
    message(STATUS "Build the coverage-tests target to generate a code coverage report for unit tests.")
endif()

# Function to compile and link a test source file (don't pass extension)
# Can also add extra sources, which should be passed with an extension.
function(graph_build_test_source NAME)
    target_sources(${TEST_TARGET} PUBLIC ${NAME}.cpp)
    foreach(EXTRA_SOURCE ${ARGN})
        target_sources(${TEST_TARGET} PUBLIC ${EXTRA_SOURCE})
    endforeach()
endfunction()

# Function to indicate that a module (source file) contains a top level test case with the given name
function(graph_add_test_case MODULE NAME)
    add_test(NAME "${MODULE}_${NAME}"
        COMMAND ${TEST_TARGET} "${NAME}")
endfunction()

graph_build_test_source(AdjacencyListGraph)

graph_add_test_case(AdjacencyListGraph graph)

graph_add_test_case(AdjacencyListGraph VecVecStringString)
graph_add_test_case(AdjacencyListGraph BidiVecVecStringString)
graph_add_test_case(AdjacencyListGraph UndirVecVecStringString)

graph_add_test_case(AdjacencyListGraph ListVecStringString)

graph_add_test_case(AdjacencyListGraph VecListStringString)
graph_add_test_case(AdjacencyListGraph BidiVecListStringString)
graph_add_test_case(AdjacencyListGraph UndirVecListStringString)

graph_build_test_source(BreadthFirstTraversal)

graph_add_test_case(BreadthFirstTraversal BFT-VecVecStringString)
graph_add_test_case(BreadthFirstTraversal BFT-BidiVecVecStringString)
graph_add_test_case(BreadthFirstTraversal BFT-UndirVecVecStringString)

graph_add_test_case(BreadthFirstTraversal BFT-ListVecStringString)

graph_add_test_case(BreadthFirstTraversal BFT-VecListStringString)
graph_add_test_case(BreadthFirstTraversal BFT-BidiVecListStringString)
graph_add_test_case(BreadthFirstTraversal BFT-UndirVecListStringString)


graph_build_test_source(ConfigMaker)

find_program(TEMPLIGHT_PROGRAM
    templight++)
find_program(TEMPLIGHT_CONVERT_PROGRAM
    templight-convert)

if(TEMPLIGHT_PROGRAM AND TEMPLIGHT_CONVERT_PROGRAM)
    set(PROFILED_SOURCE BuildBench1)
    graph_build_test_source(${PROFILED_SOURCE})

    set(PROFILED_SOURCE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/${PROFILED_SOURCE}.cpp")
    set(TEMPLIGHT_TRACE "${CMAKE_CURRENT_BINARY_DIR}/${PROFILED_SOURCE}.o.trace.pbf")
    set(CG_OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/callgrind.${PROFILED_SOURCE}.cpp")
    set(BLACKLIST "${CMAKE_CURRENT_SOURCE_DIR}/tlbl.txt")

    add_custom_target(templight-process
        "${TEMPLIGHT_PROGRAM}"
        -Xtemplight -profiler
        -Xtemplight -ignore-system
        -Xtemplight "-blacklist=${BLACKLIST}"
        -Xtemplight -safe-mode
        -std=c++17
        "-I${PROJECT_SOURCE_DIR}/include"
        -isystem "${PROJECT_SOURCE_DIR}/vendor/catch-single-header-2.4.0"
        -c "${PROFILED_SOURCE_PATH}"
        BYPRODUCTS "${TEMPLIGHT_TRACE}"
        COMMENT "Running templight on ${PROFILED_SOURCE}.cpp to profile compilation (to ${TEMPLIGHT_TRACE})"
        VERBATIM
        SOURCES "${PROFILED_SOURCE_PATH}" "${BLACKLIST}")
    add_custom_target(templight-callgrind
        "${TEMPLIGHT_CONVERT_PROGRAM}"
        -o "${CG_OUTPUT}"
        -f callgrind
        "${TEMPLIGHT_TRACE}"
        BYPRODUCTS "${CG_OUTPUT}"
        COMMENT "Converting templight trace of ${PROFILED_SOURCE}.cpp to callgrind format (to ${CG_OUTPUT})"
        VERBATIM)
    add_dependencies(templight-callgrind templight-process)

    message(STATUS "Build the templight-callgrind target to profile compilation of ${PROFILED_SOURCE}.cpp and convert output to callgrind format.")

endif()
