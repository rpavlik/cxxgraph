// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Tests for breadth-first searches in cxxgraph/Traversal.h
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

// Internal Includes
#include "Configs.h"
#include "Matcher.h"

#include "cxxgraph/AdjacencyListGraph.h"
#include "cxxgraph/Stream.h"
#include "cxxgraph/Traversal.h"
#include "cxxgraph/TraversalTrace.h"

// Library Includes
#include <catch2/catch.hpp>

// Standard Includes
#include <sstream>

namespace {
/*

Graph:

a  ->  b

|      |
v      v

c  ->  d

*/
template <typename GraphConfig>
struct BFTFixture
{

	using Graph = cxxgraph::adjacency_list::Graph<GraphConfig>;
	using BFT = cxxgraph::BreadthFirstTraversal<Graph>;
	using vertex_const_descriptor_t = cxxgraph::graph_vertex_const_descriptor_t<Graph>;
	using edge_const_descriptor_t = cxxgraph::graph_edge_const_descriptor_t<Graph>;

	BFTFixture()
	    : g(), a(g.addVertex("a")), b(g.addVertex("b")), c(g.addVertex("c")), d(g.addVertex("d")),
	      ab(g.addEdge(a, b, "ab")), ac(g.addEdge(a, c, "ac")), cd(g.addEdge(c, d, "cd")), bd(g.addEdge(b, d, "bd"))
	{}

	Graph g;
	vertex_const_descriptor_t a;
	vertex_const_descriptor_t b;
	vertex_const_descriptor_t c;
	vertex_const_descriptor_t d;

	edge_const_descriptor_t ab;
	edge_const_descriptor_t ac;
	edge_const_descriptor_t cd;
	edge_const_descriptor_t bd;

	void verifyBFTfromA(BFT const& bft) const
	{
		// a is its own predecessor
		REQUIRE(bft.predecessors.get(a) == a);
		// nothing else is its own predecessor
		REQUIRE(bft.predecessors.get(b) != b);
		REQUIRE(bft.predecessors.get(c) != c);
		REQUIRE(bft.predecessors.get(d) != d);
		// no descriptors should be default constructed.
		REQUIRE(bft.predecessors.get(b) != vertex_const_descriptor_t{});
		REQUIRE(bft.predecessors.get(c) != vertex_const_descriptor_t{});
		REQUIRE(bft.predecessors.get(d) != vertex_const_descriptor_t{});
		for (auto p : bft.predecessors.getRange()) {
			if (p.first == a) {
				// a is its own predecessor
				REQUIRE(p.first == p.second);
			} else {
				// nothing else is its own predecessor
				REQUIRE(p.first != p.second);
			}
			// no descriptors should be default constructed.
			REQUIRE_FALSE(p.first == vertex_const_descriptor_t{});
			REQUIRE_FALSE(p.second == vertex_const_descriptor_t{});
		}

		REQUIRE(bft.predecessors.get(b) == a);
		REQUIRE(bft.predecessors.get(c) == a);
		REQUIRE_THAT(bft.predecessors.get(d), IsInSet({b, c}));
	}

	void verifyBFTfromBDirected(BFT const& bft) const
	{
		CHECK(bft.predecessors.get(a) == vertex_const_descriptor_t{});
		CHECK(bft.predecessors.get(b) == b);
		CHECK(bft.predecessors.get(c) == vertex_const_descriptor_t{});
		CHECK(bft.predecessors.get(d) == b);
	}
	void verifyBFTfromBUndirected(BFT const& bft) const
	{
		REQUIRE(bft.predecessors.get(a) == b);
		REQUIRE(bft.predecessors.get(b) == b);
		REQUIRE_THAT(bft.predecessors.get(c), IsInSet({a, d}));
		REQUIRE(bft.predecessors.get(d) == b);
	}
	void verifyBFTfromDReverseDirected(BFT const& bft) const
	{
		REQUIRE_THAT(bft.predecessors.get(a), IsInSet({b, c}));
		REQUIRE(bft.predecessors.get(b) == d);
		REQUIRE(bft.predecessors.get(c) == d);
		REQUIRE(bft.predecessors.get(d) == d);
	}

	template <typename... Args, typename F>
	void doTraversal(F&& bftChecker, Args&&... args) const
	{
		INFO("a = " << a);
		INFO("b = " << b);
		INFO("c = " << c);
		INFO("d = " << d);
		INFO("ab = " << ab);
		INFO("ac = " << ac);
		INFO("cd = " << cd);
		INFO("bd = " << bd);
		{
			BFT bft{g};
			auto t = cxxgraph::trace(bft);
			traverse(g, t, std::forward<Args>(args)...);
			INFO(t.os.str());
			bftChecker(bft);
		}
		{
			BFT bft{g};
			traverse(g, bft, std::forward<Args>(args)...);
			bftChecker(bft);
		}
	}
	void testDirected() const
	{
		WHEN("Traversing as a directed graph from a")
		{
			doTraversal([&](auto& bft) { verifyBFTfromA(bft); }, a);
		}
		WHEN("Traversing as a directed graph from b")
		{
			doTraversal([&](auto& bft) { verifyBFTfromBDirected(bft); }, b);
		}
	}

	void testBidi() const
	{
		WHEN("Traversing as a directed graph from a")
		{
			doTraversal([&](auto& bft) { verifyBFTfromA(bft); }, a, cxxgraph::DirectedBehavior::Directed);
		}
		WHEN("Traversing as a directed graph from b")
		{
			doTraversal([&](auto& bft) { verifyBFTfromBDirected(bft); }, b,
			            cxxgraph::DirectedBehavior::Directed);
		}

		WHEN("Traversing bidirectionally from a")
		{
			doTraversal([&](auto& bft) { verifyBFTfromA(bft); }, a,
			            cxxgraph::DirectedBehavior::Bidirectional);
		}
		WHEN("Traversing bidirectionally from b")
		{
			doTraversal([&](auto& bft) { verifyBFTfromBUndirected(bft); }, b,
			            cxxgraph::DirectedBehavior::Bidirectional);
		}
		WHEN("Traversing bidirectionally from b, stopping when d is found")
		{
			auto checker = [&](auto& bft) {
				REQUIRE_THAT(bft.predecessors.get(a), IsInSet({b, vertex_const_descriptor_t{}}));
				REQUIRE(bft.predecessors.get(b) == b);
				REQUIRE(bft.predecessors.get(c) == vertex_const_descriptor_t{});
				REQUIRE(bft.predecessors.get(d) == b);
			};
			doTraversal(checker, b, cxxgraph::DirectedBehavior::Directed,
			            [&](auto& desc) { return desc == d; });
		}
		WHEN("Traversing reverse-directed from d")
		{
			doTraversal([&](auto& bft) { verifyBFTfromDReverseDirected(bft); }, d,
			            cxxgraph::DirectedBehavior::ReverseDirected);
		}
	}
	void testUndir() const
	{
		WHEN("Traversing from a")
		{
			doTraversal([&](auto& bft) { verifyBFTfromA(bft); }, a);
		}
		WHEN("Traversing from b")
		{
			doTraversal([&](auto& bft) { verifyBFTfromBUndirected(bft); }, b);
		}
		WHEN("Traversing from d")
		{
			doTraversal([&](auto& bft) { verifyBFTfromDReverseDirected(bft); }, d);
		}
	}
};
} // namespace

TEST_CASE_METHOD(BFTFixture<my_config::VecVecStringStringConfig>, "BFT-VecVecStringString") { testDirected(); }

TEST_CASE_METHOD(BFTFixture<my_config::BidiVecVecStringStringConfig>, "BFT-BidiVecVecStringString") { testBidi(); }

TEST_CASE_METHOD(BFTFixture<my_config::UndirVecVecStringStringConfig>, "BFT-UndirVecVecStringString") { testUndir(); }

TEST_CASE_METHOD(BFTFixture<my_config::ListVecStringStringConfig>, "BFT-ListVecStringString") { testDirected(); }

TEST_CASE_METHOD(BFTFixture<my_config::VecListStringStringConfig>, "BFT-VecListStringString") { testDirected(); }

TEST_CASE_METHOD(BFTFixture<my_config::BidiVecListStringStringConfig>, "BFT-BidiVecListStringString") { testBidi(); }

TEST_CASE_METHOD(BFTFixture<my_config::UndirVecListStringStringConfig>, "BFT-UndirVecListStringString") { testUndir(); }
